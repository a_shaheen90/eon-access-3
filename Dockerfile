FROM node:10

WORKDIR /usr/src/app

COPY package*.json ./


RUN npm install
RUN npm build

# If you are building your code for production
RUN npm ci --only=production

COPY . .

EXPOSE $PORT
CMD ["npm", "start"]