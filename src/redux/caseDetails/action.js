import { CURRENT_CASE } from "../constants";

export const getCase = (payload) => {
  return {
    type: CURRENT_CASE,
    payload,
  };
};

