import { USER_INFO } from "../constants/";

export const setUserInfo = (payload) => {
  return {
    type: USER_INFO,
    payload,
  };
};
