export const moveForward = (currentStep, lastStep, setCurrentStep) => {
  if (currentStep === lastStep) return setCurrentStep(0);
  else setCurrentStep((prevStep) => prevStep + 1);
};
