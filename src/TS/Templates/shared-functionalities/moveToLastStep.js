export const moveToLastStep = (currentStep, lastStep, setCurrentStep) => {
  if (currentStep === lastStep) return;
  setCurrentStep(() => lastStep);
};
