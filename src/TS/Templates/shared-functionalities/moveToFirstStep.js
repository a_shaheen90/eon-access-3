export const moveToFirstStep = (currentStep, setCurrentStep) => {
  if (currentStep === 0) return;

  setCurrentStep(0);
};
