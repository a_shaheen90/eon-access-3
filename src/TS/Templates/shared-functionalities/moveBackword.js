export const moveBackword = (currentStep, setCurrentStep, lastStep = 0) => {
  if (currentStep === 0) return setCurrentStep(lastStep);
  else setCurrentStep((prevStep) => prevStep - 1);
};
