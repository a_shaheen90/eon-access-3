import styles from "../milkyway-style/milkyway.module.scss";
export const stepsStyleValidation = (
  stepNumber,
  maxStepsNumber,
  lowerSteps,
  upperSteps,
  allSteps,
  startTogether,
  passiveAligners,
  overCorrectionStepsNumber
) => {
  let styleArray = [];

  // steps style
  let classList =
    allSteps - 1 < maxStepsNumber || lowerSteps === upperSteps
      ? styles.allSteps
      : styles.step;

  // passive aligners style
  let isPassiveAligners = passiveAligners
    ? styles.passiveAligners
    : styles.emptyStep;

  if (allSteps - 1 === maxStepsNumber) {
    styleArray.push(classList);
  } else {
    if (startTogether) {
      if (stepNumber < allSteps) {
        styleArray.push(classList);
      } else {
        if (overCorrectionStepsNumber <= 0) {
          styleArray.push(isPassiveAligners);
        }
      }
    } else {
      // endTogather (startTogether = false)
      if (stepNumber <= maxStepsNumber - (allSteps - 1)) {
        if (overCorrectionStepsNumber <= 0) {
          styleArray.push(isPassiveAligners);
        }
      } else {
        styleArray.push(isPassiveAligners);
      }
    }
  }

  if (stepNumber === 0) {
    styleArray.push(styles.firstStep);
  }

  if (
    stepNumber > maxStepsNumber - overCorrectionStepsNumber &&
    overCorrectionStepsNumber > 0
  ) {
    styleArray.push(styles.overCorrectionStepsNumber);
  }

  return styleArray.join(" ");
};
