import React from "react";
import styles from "../milkyway-style/milkyway.module.scss";
import images from "../../../images/milkyway-imgs/index";
import {
  moveToFirstStep,
  moveBackword,
  moveForward,
  moveToLastStep,
} from "../../shared-functionalities";
let intervalId;

export default function AnimateSteps(props) {
  const {
    tsOptions,
    setTSOptions,
    currentStep,
    setCurrentStep,
    meshesForStep,
  } = props;
  let animateStep = () => {
    let num = currentStep === meshesForStep.length - 1 ? 0 : currentStep;
    let previousStep = 0;
    intervalId = setInterval(() => {
      if (num === meshesForStep.length - 1) {
        setTSOptions((prevOptions) => ({
          ...prevOptions,
          isAnimateStart: !prevOptions.isAnimateStart,
        }));
        num = 0;
        clearInterval(intervalId);
        return;
      }
      if (num === 0) {
        setCurrentStep((prevStep) => {
          previousStep = prevStep;
          return num;
        });
      }
      setCurrentStep((pervStep) => {
        if (pervStep !== num) {
          // console.log("chcek nummmmm ", pervStep, num);
          setTSOptions((prevOptions) => ({
            ...prevOptions,
            isAnimateStart: !prevOptions.isAnimateStart,
          }));
          clearInterval(intervalId);
          return pervStep;
        }
        num += previousStep === meshesForStep.length - 1 ? 0 : 1;
        previousStep = num === 0 ? num : num - 1;
        return num;
      });
    }, tsOptions.STEPS_PAUSE || 500);
  };
  return (
    <div
      className={styles.play_pause_container}
      style={
        tsOptions.loading ||
        tsOptions.isTsPrepared ||
        !tsOptions.isTSViewerFound
          ? { zIndex: "-1" }
          : { zIndex: "1" }
      }
    >
      <img
        src={
          tsOptions.isDarkMode
            ? images.baseline_skip_previous_white
            : images.baseline_skip_previous_black
        }
        onClick={() => {
          if (tsOptions.loading || tsOptions.isTsPrepared) return;
          moveToFirstStep(currentStep, setCurrentStep);
        }}
        alt=""
      />
      <img
        src={
          tsOptions.isDarkMode
            ? images.baseline_keyboard_arrow_left_white
            : images.baseline_keyboard_arrow_left_black
        }
        alt=""
        onClick={() => {
          if (tsOptions.loading || tsOptions.isTsPrepared) return;
          moveBackword(currentStep, setCurrentStep, meshesForStep.length - 1);
        }}
      />
      <img
        src={
          tsOptions.isDarkMode
            ? tsOptions.isAnimateStart
              ? images.baseline_pause_white
              : images.baseline_play_arrow_white
            : tsOptions.isAnimateStart
            ? images.baseline_pause_black
            : images.baseline_play_arrow_black
        }
        alt=""
        onClick={() => {
          if (tsOptions.loading || tsOptions.isTsPrepared) return;
          let isAnimateStart = !tsOptions.isAnimateStart;
          if (isAnimateStart) {
            animateStep();
          } else {
            clearInterval(intervalId);
          }
          setTSOptions((prevOptions) => ({
            ...prevOptions,
            isAnimateStart: isAnimateStart,
          }));
        }}
      />
      <img
        src={
          tsOptions.isDarkMode
            ? images.baseline_keyboard_arrow_right_white
            : images.baseline_keyboard_arrow_right_black
        }
        alt=""
        onClick={() => {
          if (tsOptions.loading || tsOptions.isTsPrepared) return;
          moveForward(currentStep, meshesForStep.length - 1, setCurrentStep);
        }}
      />
      <img
        src={
          tsOptions.isDarkMode
            ? images.baseline_skip_next_white
            : images.baseline_skip_next_black
        }
        alt=""
        onClick={() => {
          if (tsOptions.loading || tsOptions.isTsPrepared) return;
          moveToLastStep(currentStep, meshesForStep.length - 1, setCurrentStep);
        }}
      />
    </div>
  );
}
