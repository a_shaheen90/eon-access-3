import React from "react";
import styles from "../milkyway-style/milkyway.module.scss";
import viewer from "../../../TS-VIEWER/viewer";
export default function DarkMode(props) {
  const {
    loading,
    isTSViewerFound,
    setTSOptions,
    renderCanvas,
    isDarkMode,
    isTsPrepared,
  } = props;

  return (
    <div className={styles.header_content}>
      <div
        className={[
          styles.toggle_dark_mode_container,
          styles.display_desktop,
        ].join(" ")}
      >
        <label className={styles.switch}>
          <input
            type="checkbox"
            disabled={loading || isTsPrepared || !isTSViewerFound}
            onClick={() => {
              setTSOptions((prevOptions) => {
                viewer.changeSceneBackgroundColor(
                  renderCanvas,
                  !prevOptions.isDarkMode
                );
                return {
                  ...prevOptions,
                  isDarkMode: !prevOptions.isDarkMode,
                };
              });
            }}
          />
          <span
            className={[styles.switch_slider, styles.round].join(" ")}
          ></span>
        </label>

        <div
          className={styles.dark_mode_title}
          style={isDarkMode ? { color: "white" } : {}}
        >
          Dark Mode
        </div>
      </div>
    </div>
  );
}
