import React, { useEffect, useState } from "react";
import styles from "../milkyway-style/milkyway.module.scss";
import ReactTooltip from "react-tooltip";
import viewer from "../../../TS-VIEWER/viewer";
import PlayPauseAnimateForLandScapeMode from "./animate-steps-for-landScape";
import images from "../../../images/milkyway-imgs/index";

export default function SideBar(props) {
  const {
    isDrawerOpen,
    setIsDrawerOpen,
    tsOptions,
    setTSOptions,
    width,
    currentStep,
    setCurrentStep,
    meshesForStep,
  } = props;

  const [viewActiveTs, setViewActiveTs] = useState("front");

  const onTsViewChange = (action) => {
    switch (action.viewActiveTs) {
      case "left-view":
        if (!tsOptions.isTSViewerFound) return;
        setViewActiveTs("left");
        setTSOptions((prevOptions) => ({
          ...prevOptions,
          showLowerArc: true,
          showUpperArc: true,
        }));
        viewer.leftView();
        break;

      case "right-view":
        if (!tsOptions.isTSViewerFound) return;
        setViewActiveTs("right");
        setTSOptions((prevOptions) => ({
          ...prevOptions,
          showLowerArc: true,
          showUpperArc: true,
        }));
        viewer.rightView();
        break;
      case "front-view":
        if (!tsOptions.isTSViewerFound) return;
        setViewActiveTs("front");
        setTSOptions((prevOptions) => ({
          ...prevOptions,
          showLowerArc: true,
          showUpperArc: true,
          showUpper: true,
          showLower: true,
        }));
        viewer.frontView();
        break;
      case "upper-view":
        if (!tsOptions.isTSViewerFound) return;
        setViewActiveTs("upper");
        setTSOptions((prevOptions) => ({
          ...prevOptions,
          showLowerArc: true,
          showUpperArc: false,
          showUpper: false,
          showLower: true,
        }));
        viewer.upperView();
        break;
      case "lower-view":
        if (!tsOptions.isTSViewerFound) return;
        setViewActiveTs("lower");
        setTSOptions((prevOptions) => ({
          ...prevOptions,
          showLowerArc: false,
          showUpperArc: true,
          showUpper: true,
          showLower: false,
        }));
        viewer.lowerView();
        break;

      case "maxilla-view":
        if (!tsOptions.isTSViewerFound) return;
        setTSOptions((prevOptions) => {
          return { ...prevOptions, showLower: !prevOptions.showLower };
        });
        break;
      case "mandible-view":
        if (!tsOptions.isTSViewerFound) return;
        setTSOptions((prevOptions) => {
          return { ...prevOptions, showUpper: !prevOptions.showUpper };
        });
        break;

      case "attachments-view":
        if (!tsOptions.isTSViewerFound) return;
        setTSOptions((prevOptions) => {
          return { ...prevOptions, isAttachment: !prevOptions.isAttachment };
        });
        break;

      case "superImpose-view":
        if (!tsOptions.isTSViewerFound) return;
        setTSOptions((prevOptions) => {
          return { ...prevOptions, isSuperImpose: !prevOptions.isSuperImpose };
        });
        break;
      default:
        if (!tsOptions.isTSViewerFound) return;
        setViewActiveTs("front");
        setTSOptions((prevOptions) => ({
          ...prevOptions,
          showLowerArc: true,
          showUpperArc: true,
        }));
        viewer.frontView();
    }
  };

  return (
    <div
      className={styles.sideBar_container}
      style={
        tsOptions.isDarkMode ? { backgroundColor: "#e5e5e5", width } : { width }
      }
    >
      <div
        className={styles.side_bar_arrow}
        onClick={() => {
          if (tsOptions.loading || tsOptions.isTsPrepared) return;
          setIsDrawerOpen((prevDrawerState) => !prevDrawerState);
        }}
        style={
          isDrawerOpen
            ? {
                justifyContent: "end",
                alignItems: "center",
                paddingRight: "20px",
              }
            : {}
        }
      >
        <img
          src={tsOptions.isDarkMode ? images.arrow_left : images.arrow_left_w}
          alt=""
          className={styles.side_bar_arrow_icon}
          style={
            isDrawerOpen
              ? {
                  transform: "scaleX(1)",
                }
              : {}
          }
        />
      </div>

      <div
        className={styles.side_bar_title}
        style={tsOptions.isDarkMode ? { color: "#090909" } : {}}
      >
        {isDrawerOpen ? "Model View" : "View"}
      </div>

      <div
        className={[
          styles.sideBar_content,
          viewActiveTs === "upper" ? styles.sideBar_content_active : "",
        ].join(" ")}
        onClick={() => {
          if (tsOptions.loading || tsOptions.isTsPrepared) return;
          onTsViewChange({ viewActiveTs: "upper-view" });
        }}
        style={isDrawerOpen ? { gridTemplateColumns: "1fr 1.4fr" } : {}}
      >
        <div>
          <img
            src={tsOptions.isDarkMode ? images.upper : images.upper_w}
            alt=""
            className={[styles.sideBar_icon].join(" ")}
            data-tip="Upper View"
          />

          <ReactTooltip
            place="right"
            effect="solid"
            disable={isDrawerOpen ? true : false}
            textColor={"white"}
            backgroundColor={"black"}
            className={styles.tooltip_override}
          />
        </div>
        <div
          className={[
            styles.sideBar_icon_title,
            isDrawerOpen ? "" : styles.hidden_element,
          ].join(" ")}
          style={tsOptions.isDarkMode ? { color: "#090909" } : {}}
        >
          {isDrawerOpen ? "Upper View" : null}
        </div>
        <div
          className={styles.sideBar_icon_title_mobile}
          style={tsOptions.isDarkMode ? { color: "#090909" } : {}}
        >
          Upper
        </div>
      </div>

      <div
        className={[
          styles.sideBar_content,
          viewActiveTs === "right" ? styles.sideBar_content_active : "",
        ].join(" ")}
        style={isDrawerOpen ? { gridTemplateColumns: "1fr 1.4fr" } : {}}
        onClick={() => {
          if (tsOptions.loading || tsOptions.isTsPrepared) return;
          onTsViewChange({ viewActiveTs: "right-view" });
        }}
      >
        <div>
          <img
            src={tsOptions.isDarkMode ? images.right : images.right_w}
            alt=""
            className={[
              styles.sideBar_icon,
              isDrawerOpen ? "" : styles.marginTop,
            ].join(" ")}
            data-tip="Right View"
          />

          <ReactTooltip
            place="right"
            effect="solid"
            disable={isDrawerOpen ? true : false}
            textColor={"white"}
            backgroundColor={"black"}
          />
        </div>
        <div
          className={[
            styles.sideBar_icon_title,
            isDrawerOpen ? "" : styles.hidden_element,
          ].join(" ")}
          style={tsOptions.isDarkMode ? { color: "#090909" } : {}}
        >
          {isDrawerOpen ? " Right View" : null}
        </div>
        <div
          className={styles.sideBar_icon_title_mobile}
          style={tsOptions.isDarkMode ? { color: "#090909" } : {}}
        >
          Right
        </div>
      </div>

      <div
        className={[
          styles.sideBar_content,
          viewActiveTs === "front" ? styles.sideBar_content_active : "",
        ].join(" ")}
        style={isDrawerOpen ? { gridTemplateColumns: "1fr 1.4fr" } : {}}
        onClick={() => {
          if (tsOptions.loading || tsOptions.isTsPrepared) return;
          onTsViewChange({ viewActiveTs: "front-view" });
        }}
      >
        <div>
          <img
            src={tsOptions.isDarkMode ? images.front : images.front_w}
            alt=""
            className={[
              styles.sideBar_icon,
              isDrawerOpen ? "" : styles.marginTop,
            ].join(" ")}
            data-tip="Front View"
          />

          <ReactTooltip
            place="right"
            effect="solid"
            disable={isDrawerOpen ? true : false}
            textColor={"white"}
            backgroundColor={"black"}
          />
        </div>
        <div
          className={[
            styles.sideBar_icon_title,
            isDrawerOpen ? "" : styles.hidden_element,
          ].join(" ")}
          style={tsOptions.isDarkMode ? { color: "#090909" } : {}}
        >
          {isDrawerOpen ? "Front View" : null}
        </div>
        <div
          className={styles.sideBar_icon_title_mobile}
          style={tsOptions.isDarkMode ? { color: "#090909" } : {}}
        >
          Front
        </div>
      </div>
      <div
        className={[
          styles.sideBar_content,
          viewActiveTs === "left" ? styles.sideBar_content_active : "",
        ].join(" ")}
        style={isDrawerOpen ? { gridTemplateColumns: "1fr 1.4fr" } : {}}
        onClick={() => {
          if (tsOptions.loading || tsOptions.isTsPrepared) return;
          onTsViewChange({ viewActiveTs: "left-view" });
        }}
      >
        <div>
          <img
            src={tsOptions.isDarkMode ? images.left : images.left_w}
            alt=""
            className={[
              styles.sideBar_icon,
              isDrawerOpen ? "" : styles.marginTop,
            ].join(" ")}
            data-tip="Left View"
          />

          <ReactTooltip
            place="right"
            effect="solid"
            disable={isDrawerOpen ? true : false}
            textColor={"white"}
            backgroundColor={"black"}
          />
        </div>
        <div
          className={[
            styles.sideBar_icon_title,
            isDrawerOpen ? "" : styles.hidden_element,
          ].join(" ")}
          style={tsOptions.isDarkMode ? { color: "#090909" } : {}}
        >
          {isDrawerOpen ? "Left View" : null}
        </div>
        <div
          className={styles.sideBar_icon_title_mobile}
          style={tsOptions.isDarkMode ? { color: "#090909" } : {}}
        >
          Left
        </div>
      </div>

      <div
        className={[
          styles.sideBar_content,
          viewActiveTs === "lower" ? styles.sideBar_content_active : "",
        ].join(" ")}
        style={isDrawerOpen ? { gridTemplateColumns: "1fr 1.4fr" } : {}}
        onClick={() => {
          if (tsOptions.loading || tsOptions.isTsPrepared) return;
          onTsViewChange({ viewActiveTs: "lower-view" });
        }}
      >
        <div>
          <img
            src={tsOptions.isDarkMode ? images.lower : images.lower_w}
            alt=""
            className={[
              styles.sideBar_icon,
              isDrawerOpen ? "" : styles.marginTop,
            ].join(" ")}
            data-tip="Lower View"
          />

          <ReactTooltip
            place="right"
            effect="solid"
            disable={isDrawerOpen ? true : false}
            textColor={"white"}
            backgroundColor={"black"}
          />
        </div>
        <div
          className={[
            styles.sideBar_icon_title,
            isDrawerOpen ? "" : styles.hidden_element,
          ].join(" ")}
          style={tsOptions.isDarkMode ? { color: "#090909" } : {}}
        >
          {isDrawerOpen ? "Lower View" : null}
        </div>
        <div
          className={styles.sideBar_icon_title_mobile}
          style={tsOptions.isDarkMode ? { color: "#090909" } : {}}
        >
          Lower
        </div>
      </div>

      <div
        className={styles.side_bar_details_title}
        style={tsOptions.isDarkMode ? { color: "#090909" } : {}}
      >
        {isDrawerOpen ? "Model Details" : "Model"}
      </div>

      {/* Model Details */}
      <div
        className={[
          styles.sideBar_content,
          tsOptions.showLower ? styles.sideBar_content_active : "",
        ].join(" ")}
        onClick={() => {
          if (tsOptions.loading || tsOptions.isTsPrepared) return;
          onTsViewChange({ viewActiveTs: "maxilla-view" });
        }}
        style={isDrawerOpen ? { gridTemplateColumns: "1fr 1.4fr" } : {}}
      >
        <div>
          <img
            src={
              tsOptions.isDarkMode ? images.active_upper : images.active_upper_w
            }
            alt=""
            className={[styles.sideBar_icon].join(" ")}
            data-tip="Maxilla"
          />

          <ReactTooltip
            place="right"
            effect="solid"
            disable={isDrawerOpen ? true : false}
            textColor={"white"}
            backgroundColor={"black"}
            className={styles.tooltip_override}
          />
        </div>
        <div
          className={[
            styles.sideBar_icon_title,
            isDrawerOpen ? "" : styles.hidden_element,
          ].join(" ")}
          style={tsOptions.isDarkMode ? { color: "#090909" } : {}}
        >
          {isDrawerOpen ? "Maxilla" : null}
        </div>
        <div
          className={styles.sideBar_icon_title_mobile}
          style={tsOptions.isDarkMode ? { color: "#090909" } : {}}
        >
          Maxilla
        </div>
      </div>

      {/* Mandible */}
      <div
        className={[
          styles.sideBar_content,
          tsOptions.showUpper ? styles.sideBar_content_active : "",
        ].join(" ")}
        onClick={() => {
          if (tsOptions.loading || tsOptions.isTsPrepared) return;
          onTsViewChange({ viewActiveTs: "mandible-view" });
        }}
        style={isDrawerOpen ? { gridTemplateColumns: "1fr 1.4fr" } : {}}
      >
        <div>
          <img
            src={
              tsOptions.isDarkMode ? images.active_lower : images.active_lower_w
            }
            alt=""
            className={[styles.sideBar_icon].join(" ")}
            data-tip="Mandible"
          />

          <ReactTooltip
            place="right"
            effect="solid"
            disable={isDrawerOpen ? true : false}
            textColor={"white"}
            backgroundColor={"black"}
            className={styles.tooltip_override}
          />
        </div>
        <div
          className={[
            styles.sideBar_icon_title,
            isDrawerOpen ? "" : styles.hidden_element,
          ].join(" ")}
          style={tsOptions.isDarkMode ? { color: "#090909" } : {}}
        >
          {isDrawerOpen ? "Mandible" : null}
        </div>
        <div
          className={styles.sideBar_icon_title_mobile}
          style={tsOptions.isDarkMode ? { color: "#090909" } : {}}
        >
          Mandible
        </div>
      </div>

      {/* Attachments */}

      <div
        className={[
          styles.sideBar_content,
          styles.display_desktop,
          tsOptions.isAttachment ? styles.sideBar_content_active : "",
        ].join(" ")}
        onClick={() => {
          if (tsOptions.loading || tsOptions.isTsPrepared) return;
          onTsViewChange({ viewActiveTs: "attachments-view" });
        }}
        style={isDrawerOpen ? { gridTemplateColumns: "1fr 1.4fr" } : {}}
      >
        <div>
          <img
            src={
              tsOptions.isDarkMode ? images.attachments : images.attachments_w
            }
            alt=""
            className={[styles.sideBar_icon].join(" ")}
            data-tip="Attachments"
          />

          <ReactTooltip
            place="right"
            effect="solid"
            disable={isDrawerOpen ? true : false}
            textColor={"white"}
            backgroundColor={"black"}
            className={styles.tooltip_override}
          />
        </div>
        <div
          className={[
            styles.sideBar_icon_title,
            isDrawerOpen ? "" : styles.hidden_element,
          ].join(" ")}
          style={tsOptions.isDarkMode ? { color: "#090909" } : {}}
        >
          {isDrawerOpen ? "Attachments" : null}
        </div>
        <div
          className={styles.sideBar_icon_title_mobile}
          style={tsOptions.isDarkMode ? { color: "#090909" } : {}}
        >
          Attachments
        </div>
      </div>

      {/* Super Impose */}

      <div
        className={[
          styles.sideBar_content,
          styles.display_desktop,
          tsOptions.isSuperImpose ? styles.sideBar_content_active : "",
        ].join(" ")}
        onClick={() => {
          if (tsOptions.loading || tsOptions.isTsPrepared) return;
          onTsViewChange({ viewActiveTs: "superImpose-view" });
        }}
        style={isDrawerOpen ? { gridTemplateColumns: "1fr 1.4fr" } : {}}
      >
        <div>
          <img
            src={tsOptions.isDarkMode ? images.Super : images.super_w}
            alt=""
            className={[styles.sideBar_icon].join(" ")}
            data-tip="Super Impose"
          />

          <ReactTooltip
            place="right"
            effect="solid"
            disable={isDrawerOpen ? true : false}
            textColor={"white"}
            backgroundColor={"black"}
            className={styles.tooltip_override}
          />
        </div>
        <div
          className={[
            styles.sideBar_icon_title,
            isDrawerOpen ? "" : styles.hidden_element,
          ].join(" ")}
          style={tsOptions.isDarkMode ? { color: "#090909" } : {}}
        >
          {isDrawerOpen ? "Super Impose" : null}
        </div>
        <div
          className={styles.sideBar_icon_title_mobile}
          style={tsOptions.isDarkMode ? { color: "#090909" } : {}}
        >
          {" "}
          Super
        </div>
      </div>

      {/* <!-- Play/Pause animate only for landscape mode--> */}
      <PlayPauseAnimateForLandScapeMode
        tsOptions={tsOptions}
        setTSOptions={setTSOptions}
        currentStep={currentStep}
        setCurrentStep={setCurrentStep}
        meshesForStep={meshesForStep}
      />

      <div className={styles.empty_div_onMobile} />
    </div>
  );
}
