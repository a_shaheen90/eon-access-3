import React from "react";
import styles from "../milkyway-style/milkyway.module.scss";
import viewer from "../../../TS-VIEWER/viewer";
export default function ModalSettings(props) {
  const { tsOptions, setTSOptions, renderCanvas, onCloseModal } = props;
  return (
    // <!-- The Modal -->
    <div className={styles.modal_hidden_on_desktop}>
      {/* <!-- Modal content --> */}
      <div className={styles.modal_content}>
        <span
          onClick={onCloseModal}
          className={[styles.close, "pull-right p-2"].join(" ")}
        >
          <i className="fa fa-times icon-md2" />
        </span>
        <div className={styles.modal_title}>More Settings</div>

        <div className={styles.modal_switch_container}>
          <div
            style={{
              paddingLeft: 10,
              cursor: "auto",
              color: "#000",
              textAlign: "start",
            }}
          >
            Dark Mode
          </div>
          <div className={styles.side_line}></div>
          <label className={styles.switch}>
            <input
              type="checkbox"
              defaultChecked={tsOptions.isDarkMode}
              onClick={() => {
                setTSOptions((prevOptions) => {
                  viewer.changeSceneBackgroundColor(
                    renderCanvas,
                    !prevOptions.isDarkMode
                  );
                  return {
                    ...prevOptions,
                    isDarkMode: !prevOptions.isDarkMode,
                  };
                });
              }}
            />
            <span
              className={[styles.switch_slider, styles.round].join(" ")}
            ></span>
          </label>
        </div>

        <div className={styles.side_bottom}></div>

        <div className={styles.modal_switch_container}>
          <div
            style={{
              paddingLeft: 10,
              cursor: "auto",
              color: "#000",
              textAlign: "start",
            }}
          >
            Attachments
          </div>
          <div className={styles.side_line}></div>
          <label className={styles.switch}>
            <input
              type="checkbox"
              defaultChecked={tsOptions.isAttachment}
              onClick={() =>
                setTSOptions((prevOptions) => {
                  return {
                    ...prevOptions,
                    isAttachment: !prevOptions.isAttachment,
                  };
                })
              }
            />
            <span
              className={[styles.switch_slider, styles.round].join(" ")}
            ></span>
          </label>
        </div>

        <div className={styles.side_bottom}></div>

        <div className={styles.modal_switch_container}>
          <div
            style={{
              paddingLeft: 10,
              cursor: "auto",
              color: "#000",
              textAlign: "start",
            }}
          >
            Super Impose
          </div>
          <div className={styles.side_line}></div>
          <label className={styles.switch}>
            <input
              type="checkbox"
              defaultChecked={tsOptions.isSuperImpose}
              onClick={() =>
                setTSOptions((prevOptions) => {
                  return {
                    ...prevOptions,
                    isSuperImpose: !prevOptions.isSuperImpose,
                  };
                })
              }
            />
            <span
              className={[styles.switch_slider, styles.round].join(" ")}
            ></span>
          </label>
        </div>
      </div>
    </div>
  );
}
