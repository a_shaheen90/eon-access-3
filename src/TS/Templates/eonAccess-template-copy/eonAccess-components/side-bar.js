import React, { useEffect, useState } from "react";
import styles from "../eonAccess-style/eonAccess.module.scss";
import viewer from "../../../TS-VIEWER/viewer";
import PlayPauseAnimateForLandScapeMode from "./animate-steps-for-landScape";
export default function SideBar(props) {
  const {
    isDrawerOpen,
    setIsDrawerOpen,
    tsOptions,
    setTSOptions,
    width,
    currentStep,
    setCurrentStep,
    meshesForStep,
  } = props;

  const [viewActiveTs, setViewActiveTs] = useState("front");
  const [viewUpper, setViewUpper] = useState(true);
  const [viewLower, setViewLower] = useState(true);
  const [viewAttachments, setViewAttachments] = useState(true);
  const [viewLayers, setViewLayers] = useState(false);

  const onTsViewChange = (action) => {
    switch (action.viewActiveTs) {
      case "left-view":
        if (!tsOptions.isTSViewerFound) return;
        setViewActiveTs("left");
        setTSOptions((prevOptions) => ({
          ...prevOptions,
          showLowerArc: true,
          showUpperArc: true,
        }));
        viewer.leftView();
        break;

      case "right-view":
        if (!tsOptions.isTSViewerFound) return;
        setViewActiveTs("right");
        setTSOptions((prevOptions) => ({
          ...prevOptions,
          showLowerArc: true,
          showUpperArc: true,
        }));
        viewer.rightView();
        break;
      case "front-view":
        if (!tsOptions.isTSViewerFound) return;
        setViewActiveTs("front");
        setTSOptions((prevOptions) => ({
          ...prevOptions,
          showLowerArc: true,
          showUpperArc: true,
          showUpper: true,
          showLower: true,
        }));
        viewer.frontView();
        break;
      case "upper-view":
        if (!tsOptions.isTSViewerFound) return;
        setViewActiveTs("upper");
        setTSOptions((prevOptions) => ({
          ...prevOptions,
          showLowerArc: true,
          showUpperArc: false,
          showUpper: false,
          showLower: true,
        }));
        viewer.upperView();
        break;
      case "lower-view":
        if (!tsOptions.isTSViewerFound) return;
        setViewActiveTs("lower");
        setTSOptions((prevOptions) => ({
          ...prevOptions,
          showLowerArc: false,
          showUpperArc: true,
          showUpper: true,
          showLower: false,
        }));
        viewer.lowerView();
        break;

      case "maxilla-view":
        if (!tsOptions.isTSViewerFound) return;
        setTSOptions((prevOptions) => {
          return { ...prevOptions, showLower: !prevOptions.showLower };
        });
        break;
      case "mandible-view":
        if (!tsOptions.isTSViewerFound) return;
        setTSOptions((prevOptions) => {
          return { ...prevOptions, showUpper: !prevOptions.showUpper };
        });
        break;

      case "attachments-view":
        if (!tsOptions.isTSViewerFound) return;
        setTSOptions((prevOptions) => {
          return { ...prevOptions, isAttachment: !prevOptions.isAttachment };
        });
        break;

      case "superImpose-view":
        if (!tsOptions.isTSViewerFound) return;
        setTSOptions((prevOptions) => {
          return { ...prevOptions, isSuperImpose: !prevOptions.isSuperImpose };
        });
        break;
      default:
        if (!tsOptions.isTSViewerFound) return;
        setViewActiveTs("front");
        setTSOptions((prevOptions) => ({
          ...prevOptions,
          showLowerArc: true,
          showUpperArc: true,
        }));
        viewer.frontView();
    }
  };

  return (
    <div
      className={styles.sideBar_container}
    >
      <div
        className={styles.sideBar_content}
        onClick={() => {
          if (tsOptions.loading || tsOptions.isTsPrepared) return;
          onTsViewChange({ viewActiveTs: "upper-view" });
        }}
      >
        <div>
          <img
           src={`${process.env.REACT_APP_IMAGE_URL}/eonAccess-imgs/${ viewActiveTs === "upper" ? "top_active.svg": "top.svg"}`}
            alt=""
            className={[styles.sideBar_icon].join(" ")}
            data-tip="Upper View"
          />

        </div>
        <div
         className={[
          styles.upperNav_content_moblie,
          viewActiveTs === "upper" ? styles.upperNav_content_active : "",
        ].join(" ")}
        >
          Top
        </div>

      </div>

      <div
        className={styles.sideBar_content}
        style={isDrawerOpen ? { gridTemplateColumns: "1fr 1.4fr" } : {}}
        onClick={() => {
          if (tsOptions.loading || tsOptions.isTsPrepared) return;
          onTsViewChange({ viewActiveTs: "right-view" });
        }}
      >
        <div>
          <img
            src={`${process.env.REACT_APP_IMAGE_URL}/eonAccess-imgs/${ viewActiveTs === "right" ? "right_active.svg": "right.svg"}`}
            alt=""
            className={styles.sideBar_icon}
            data-tip="Right View"
          />
        </div>
        <div
         className={[
          styles.upperNav_content_moblie,
          viewActiveTs === "right" ? styles.upperNav_content_active : "",
        ].join(" ")}
        >
          Right
        </div>
      </div>

      <div
        className={
          styles.sideBar_content
        }
        style={isDrawerOpen ? { gridTemplateColumns: "1fr 1.4fr" } : {}}
        onClick={() => {
          if (tsOptions.loading || tsOptions.isTsPrepared) return;
          onTsViewChange({ viewActiveTs: "front-view" });
        }}
      >
        <div>
          <img
            src={`${process.env.REACT_APP_IMAGE_URL}/eonAccess-imgs/${ viewActiveTs === "front" ? "front_active.svg": "front.svg"}`}
            alt=""
            className={[
              styles.sideBar_icon,
              isDrawerOpen ? "" : styles.marginTop,
            ].join(" ")}
            data-tip="Front View"
          />
        </div>
        <div
          className={[
            styles.upperNav_content_moblie,
            viewActiveTs === "front" ? styles.upperNav_content_active : "",
          ].join(" ")}
        >
          Front
        </div>
      </div>
      <div
        className={styles.sideBar_content}
        style={isDrawerOpen ? { gridTemplateColumns: "1fr 1.4fr" } : {}}
        onClick={() => {
          if (tsOptions.loading || tsOptions.isTsPrepared) return;
          onTsViewChange({ viewActiveTs: "left-view" });
        }}
      >
        <div>
          <img
             src={`${process.env.REACT_APP_IMAGE_URL}/eonAccess-imgs/${ viewActiveTs === "left" ? "left_active.svg": "left.svg"}`}
            alt=""
            className={[
              styles.sideBar_icon,
              isDrawerOpen ? "" : styles.marginTop,
            ].join(" ")}
            data-tip="Left View"
          />
        </div>
        <div
          className={[
            styles.sideBar_icon_title,
            isDrawerOpen ? "" : styles.hidden_element,
          ].join(" ")}
          style={tsOptions.isDarkMode ? { color: "#090909" } : {}}
        >
          {isDrawerOpen ? "Left View" : null}
        </div>
        <div
           className={[
            styles.upperNav_content_moblie,
            viewActiveTs === "left" ? styles.upperNav_content_active : "",
          ].join(" ")}
        >
          Left
        </div>
      </div>

      <div
        className={styles.sideBar_content}
        style={isDrawerOpen ? { gridTemplateColumns: "1fr 1.4fr" } : {}}
        onClick={() => {
          if (tsOptions.loading || tsOptions.isTsPrepared) return;
          onTsViewChange({ viewActiveTs: "lower-view" });
        }}
      >
        <div>
          <img
             src={`${process.env.REACT_APP_IMAGE_URL}/eonAccess-imgs/${ viewActiveTs === "lower" ? "bottom_active.svg": "bottom.svg"}`}
            alt=""
            className={[
              styles.sideBar_icon,
              isDrawerOpen ? "" : styles.marginTop,
            ].join(" ")}
            data-tip="Lower View"
          />
        </div>
        <div
          className={[
            styles.upperNav_content_moblie,
            viewActiveTs === "lower" ? styles.upperNav_content_active : "",
          ].join(" ")}
        >
          Bottom
        </div>
      </div>

      <div
        className={styles.side_bar_details_title}
        style={tsOptions.isDarkMode ? { color: "#090909" } : {}}
      >
        {isDrawerOpen ? "Model Details" : "Model"}
      </div>

      {/* Model Details */}
      <div
        className={styles.sideBar_content}
        onClick={() => {
          if (tsOptions.loading || tsOptions.isTsPrepared) return;
          onTsViewChange({ viewActiveTs: "maxilla-view" });
          setViewUpper(!viewUpper)
        }}
        style={isDrawerOpen ? { gridTemplateColumns: "1fr 1.4fr" } : {}}
      >
        <div>
          <img
             src={
              `${process.env.REACT_APP_IMAGE_URL}/eonAccess-imgs/${viewUpper ? "upper_active.svg" : "upper.svg"}`
          }
            alt=""
            className={[styles.sideBar_icon_upper].join(" ")}
            data-tip="Maxilla"
          />
        </div>
        <div
         className={[
          styles.upperNav_content_moblie,
          viewUpper ? styles.upperNav_content_active : "",
        ].join(" ")}
        >
          Upper
        </div>
      </div>

      {/* Mandible */}
      <div
        className={styles.sideBar_content}
        onClick={() => {
          if (tsOptions.loading || tsOptions.isTsPrepared) return;
          onTsViewChange({ viewActiveTs: "mandible-view" });
          setViewLower(!viewLower)
        }}
        style={isDrawerOpen ? { gridTemplateColumns: "1fr 1.4fr" } : {}}
      >
        <div>
          <img
            src={
              `${process.env.REACT_APP_IMAGE_URL}/eonAccess-imgs/${viewLower ? "lower_active.svg" : "lower.svg"}` 
          }
            alt=""
            className={[styles.sideBar_icon_lower].join(" ")}
            data-tip="Mandible"
          />
        </div>
        <div
         className={[
          styles.upperNav_content_moblie,
          viewLower ? styles.upperNav_content_active : "",
        ].join(" ")}
        >
          Lower
        </div>
      </div>

      {/* <!-- Play/Pause animate only for landscape mode--> */}
      <PlayPauseAnimateForLandScapeMode
        tsOptions={tsOptions}
        setTSOptions={setTSOptions}
        currentStep={currentStep}
        setCurrentStep={setCurrentStep}
        meshesForStep={meshesForStep}
      />
      <div className={styles.empty_div_onMobile} />
    </div>
  );
}
