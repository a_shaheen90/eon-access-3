import React, { useMemo } from "react";
import styles from "../eonAccess-style/eonAccess.module.scss";
export default function Header(props) {
  const { currentStep, stepsOptions, isDarkMode } = props;
  const handleSubHeader = useMemo(() => {
    const { lowerSteps, upperSteps, startTogether } = stepsOptions;
    if (lowerSteps === upperSteps) return "Upper & Lower";

    if (startTogether) {
      if (upperSteps > lowerSteps) {
        return currentStep < lowerSteps ? "Upper & Lower" : "Upper Only";
      } else {
        return currentStep < upperSteps ? "Upper & Lower" : "Lower Only";
      }
    } else {
      if (upperSteps > lowerSteps) {
        let delta = upperSteps - lowerSteps;

        return currentStep < delta ? "Upper Only" : "Upper & Lower";
      } else {
        let delta = lowerSteps - upperSteps;

        return currentStep < delta ? "Lower Only" : "Upper & Lower";
      }
    }
  }, [currentStep]);
  return (
    <React.Fragment>
      <header
        className={styles.viewer_window_header}
        style={isDarkMode ? { color: "white" } : {}}
      >
        {currentStep === 0 ? "Current" : `Step ${currentStep}`}
      </header>
      <div
        className={styles.viewer_window_sub_header}
        style={isDarkMode ? { color: "white" } : {}}
      >
        {currentStep === 0 ? "" : handleSubHeader}
      </div>
    </React.Fragment>
  );
}
