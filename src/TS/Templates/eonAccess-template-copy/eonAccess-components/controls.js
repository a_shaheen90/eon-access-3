import React from "react";
import styles from "../eonAccess-style/eonAccess.module.scss";
import viewer from "../../../TS-VIEWER/viewer";
export default function Controls(props) {
  const {
    loading,
    isTSViewerFound,
    setTSOptions,
    isTsPrepared,
    revise,
    setRevise
  } = props;

  return (
    <div className={styles.header_content}>
      <div
        className={[
          styles.toggle_dark_mode_container,
          styles.display_desktop,
        ].join(" ")}
      >
        <label className={styles.switch}>
          <input
            type="checkbox"
            disabled={loading || isTsPrepared || !isTSViewerFound}
            onClick={() => {
              setTSOptions((prevOptions) => {
                viewer.toggleCamera();
                return {
                  ...prevOptions,
                };
              });
              setRevise(() => !revise)
            }}
          />
          <span
            className={[styles.switch_slider, styles.round].join(" ")}
          ></span>
        </label>

        <div
          className={styles.dark_mode_title}
        >
          Disable Controls
        </div>
      </div>
    </div>
  );
}
