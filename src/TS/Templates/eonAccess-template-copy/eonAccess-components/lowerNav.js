import React, { useEffect, useState } from "react";
import styles from "../eonAccess-style/eonAccess.module.scss";
import viewer from "../../../TS-VIEWER/viewer";
export default function LowerNav(props) {
  const {
    isDrawerOpen,
    tsOptions,
    setTSOptions,
  } = props;

  const [viewActiveTs, setViewActiveTs] = useState("front");
  const [viewUpper, setViewUpper] = useState(true);
  const [viewLower, setViewLower] = useState(true);
  const [viewAttachments, setViewAttachments] = useState(true);
  const [viewLayers, setViewLayers] = useState(false);


  const onTsViewChange = (action) => {
    switch (action.viewActiveTs) {
      case "maxilla-view":
        if (!tsOptions.isTSViewerFound) return;
        setTSOptions((prevOptions) => {
          return { ...prevOptions, showLower: !prevOptions.showLower };
        });
        break;
      case "mandible-view":
        if (!tsOptions.isTSViewerFound) return;
        setTSOptions((prevOptions) => {
          return { ...prevOptions, showUpper: !prevOptions.showUpper };
        });
        break;

      case "attachments-view":
        if (!tsOptions.isTSViewerFound) return;
        setTSOptions((prevOptions) => {
          return { ...prevOptions, isAttachment: !prevOptions.isAttachment };
        });
        break;

      case "superImpose-view":
        if (!tsOptions.isTSViewerFound) return;
        setTSOptions((prevOptions) => {
          return { ...prevOptions, isSuperImpose: !prevOptions.isSuperImpose };
        });
        break;
      default:
        if (!tsOptions.isTSViewerFound) return;
        setViewActiveTs("front");
        setTSOptions((prevOptions) => ({
          ...prevOptions,
          showLowerArc: true,
          showUpperArc: true,
        }));
        viewer.frontView();
    }
  };

  return (
    <div
      className={styles.lowerNav_container}
    >
    <div onClick={() => {
          if (tsOptions.loading || tsOptions.isTsPrepared) return;
          onTsViewChange({ viewActiveTs: "maxilla-view" });
          setViewUpper(!viewUpper);
        }}>
      <div
        className={[
          styles.lowerNav_content,
          tsOptions.showLower ? styles.lowerNav_content_active : "",
        ].join(" ")}
        
      >
      <div>
        <img
          src={
              `${process.env.REACT_APP_IMAGE_URL}/eonAccess-imgs/${viewUpper ? "upper_active.svg" : "upper.svg"}`
          }
          alt=""
          className={[styles.lowerNav_icon_upper].join(" ")}
          data-tip="Maxilla"
          />

       
        </div>
        <div
          className={[
            styles.lowerNav_icon_title,
            isDrawerOpen ? "" : styles.hidden_element,
          ].join(" ")}     
        >
          {isDrawerOpen ? "Maxilla" : null}
        </div>
        <div
          className={styles.lowerNav_icon_title}
        >
          Upper Arch
        </div>
      </div>
        </div>
     <div>
       {/* Mandible */}
      <div
        className={[
          styles.lowerNav_content,
          tsOptions.showUpper ? styles.lowerNav_content_active : "",
        ].join(" ")}
        onClick={() => {
          if (tsOptions.loading || tsOptions.isTsPrepared) return;
          onTsViewChange({ viewActiveTs: "mandible-view" });
          setViewLower(!viewLower)
        }}
        style={isDrawerOpen ? { gridTemplateColumns: "1fr 1.4fr" } : {}}
      >
        <div>
          <img
            src={
                `${process.env.REACT_APP_IMAGE_URL}/eonAccess-imgs/${viewLower ? "lower_active.svg" : "lower.svg"}` 
            }
            alt=""
            className={[styles.lowerNav_icon_lower].join(" ")}
            data-tip="Mandible"
          />

         
        </div>
        <div
          className={[
            styles.lowerNav_icon_title,
            isDrawerOpen ? "" : styles.hidden_element,
          ].join(" ")}
          style={tsOptions.isDarkMode ? { color: "#090909" } : {}}
        >
          {isDrawerOpen ? "Mandible" : null}
        </div>
        <div
          className={styles.lowerNav_icon_title}
        >
          Lower Arch
        </div>
      </div>
     </div>
     <div>
       {/* Attachments */}

      <div
        className={[
          styles.lowerNav_content,
          styles.display_desktop,
          tsOptions.isAttachment ? styles.lowerNav_content_active : "",
        ].join(" ")}
        onClick={() => {
          if (tsOptions.loading || tsOptions.isTsPrepared) return;
          onTsViewChange({ viewActiveTs: "attachments-view" });
          setViewAttachments(!viewAttachments)
        }}
        style={isDrawerOpen ? { gridTemplateColumns: "1fr 1.4fr" } : {}}
      >
        <div>
          <img
            src={
               `${process.env.REACT_APP_IMAGE_URL}/eonAccess-imgs/${viewAttachments ? "attachments_active.svg" : "attachments.svg"}`
            }
            alt=""
            className={[styles.lowerNav_icon].join(" ")}
            data-tip="Attachments"
          />

        
        </div>
        <div
          className={[
            styles.lowerNav_icon_title,
            isDrawerOpen ? "" : styles.hidden_element,
          ].join(" ")}
        >
          {isDrawerOpen ? "Attachments" : null}
        </div>
        <div
          className={styles.lowerNav_icon_title}
        >
          Attachments
        </div>
      </div>

     </div>
     <div>
        
      {/* Super Impose */}

      <div
        className={[
          styles.lowerNav_content,
          styles.display_desktop,
          tsOptions.isSuperImpose ? styles.lowerNav_content_active : "",
        ].join(" ")}
        onClick={() => {
          if (tsOptions.loading || tsOptions.isTsPrepared) return;
          onTsViewChange({ viewActiveTs: "superImpose-view" });
          setViewLayers(!viewLayers)
        }}
        style={isDrawerOpen ? { gridTemplateColumns: "1fr 1.4fr" } : {}}
      >
        <div>
          <img
            src={
                `${process.env.REACT_APP_IMAGE_URL}/eonAccess-imgs/${viewLayers ? "layers_active.svg" : "layers.svg"}`
            }
            alt=""
            className={[styles.lowerNav_icon].join(" ")}
            data-tip="Super Impose"
          />

         
        </div>
        <div
          className={[
            styles.lowerNav_icon_title,
            isDrawerOpen ? "" : styles.hidden_element,
          ].join(" ")}
         
        >
          {isDrawerOpen ? "Super Impose" : null}
        </div>
        <div
          className={styles.lowerNav_icon_title}
        >
          {" "}
          Superimpose
        </div>
      </div>
      <div className={styles.empty_div_onMobile} />
    </div>
     </div>
  );
}
