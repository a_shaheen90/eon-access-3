import * as yup from "yup";
export const schema = yup.object({
  treatment_type: yup
    .string()
    .required("Please choose one of Treatment Types "),
});

export const hybridSchema = yup.object({
  type: yup.string().required("Please choose one of Hybrid Treatment Types "),
  aligners: yup.string().required("Please choose one "),
  //  braces: yup.string().required("Please choose one "),
});
