export const caseType = {
  LiteII: "Lite",
  UnlimitedII: "Unlimited",
  "Lite Single Arch": "Lite Single Arch",
  Retainer: "Retainer",
};
