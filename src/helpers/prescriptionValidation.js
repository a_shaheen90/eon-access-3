import * as yup from "yup";
export const schema = yup.object({
  chief_complaint: yup.string().required("Chief Complaint is Required "),
  summary_and_special_instructions: yup.string().required("summary and special instructions is Required "),
  treat_arches: yup.string().required("Treat Arches is Required"),
  wear_cycle: yup.string().required("Wear Cycle is Required"),
});
