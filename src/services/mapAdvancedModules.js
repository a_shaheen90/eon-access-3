import { modules } from "../redux/advanced/modules";
export function mapModules(data) {
    for (var x = 0; x < modules.length; x++) {
        var module = data?.filter((value) => { return value.key === modules[x].key })||[]
        modules[x].active = module[0]?.active || false
    }

    return modules
}