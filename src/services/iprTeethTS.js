import { lower_ipr_teeth, upper_ipr_teeth } from "../redux/customeTeeth/init_ipr_teeth"

export function iprTeethTS() {
    const teeth = {
        upper_teeth: Object.assign({}, upper_ipr_teeth),
        lower_teeth: Object.assign({}, lower_ipr_teeth),
    }
    return teeth
}