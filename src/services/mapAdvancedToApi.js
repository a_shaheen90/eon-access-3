export function mapAdvancedToApi(data) {
    const result = data.map(({ active_img, img, id, title, ...rest }) => ({ ...rest }));
    return result
}