export const ipr =
{
    "lower_teeth": {
        "lower_r_8": {
            "value": false,
            "index": 8,
            "amount": "",
            "before_step": "",
            "divider": true,
            "use_ipr": false,
            "do_not_perform_ipr": true,
            "fdi_key": 48,
            "tooth_key": "LR_8 / LR_7"
        },
        "lower_r_7": {
            "value": false,
            "index": 7,
            "amount": "",
            "before_step": "",
            "divider": true,
            "use_ipr": false,
            "do_not_perform_ipr": true,
            "fdi_key": 47,
            "tooth_key": "LR_7 / LR_6"
        },
        "lower_r_6": {
            "value": false,
            "index": 6,
            "amount": "",
            "before_step": "",
            "divider": true,
            "use_ipr": false,
            "do_not_perform_ipr": true,
            "fdi_key": 46,
            "tooth_key": "LR_6 / LR_5"
        },
        "lower_r_5": {
            "value": false,
            "index": 5,
            "amount": 0.3,
            "before_step": 10,
            "divider": true,
            "use_ipr": true,
            "do_not_perform_ipr": true,
            "fdi_key": 45,
            "tooth_key": "LR_5 / LR_4"
        },
        "lower_r_4": {
            "value": false,
            "index": 4,
            "amount": "",
            "before_step": "",
            "divider": true,
            "use_ipr": false,
            "do_not_perform_ipr": true,
            "fdi_key": 44,
            "tooth_key": "LR_4 / LR_3"
        },
        "lower_r_3": {
            "value": false,
            "index": 3,
            "amount": 0.9,
            "before_step": 9,
            "divider": true,
            "use_ipr": true,
            "do_not_perform_ipr": true,
            "fdi_key": 43,
            "tooth_key": "LR_3 / LR_2"
        },
        "lower_r_2": {
            "value": false,
            "index": 2,
            "amount": "",
            "before_step": "",
            "divider": true,
            "use_ipr": false,
            "do_not_perform_ipr": true,
            "fdi_key": 42,
            "tooth_key": "LR_2 / LR_1"
        },
        "lower_r_1": {
            "value": false,
            "index": 1,
            "amount": "",
            "before_step": "",
            "divider": true,
            "use_ipr": false,
            "do_not_perform_ipr": true,
            "fdi_key": 41,
            "tooth_key": "LR_1 / LR_0"
        },
        "lower_l_1": {
            "value": false,
            "index": 1,
            "amount": 0.3,
            "before_step": 5,
            "divider": true,
            "use_ipr": true,
            "do_not_perform_ipr": true,
            "tooth_key": "LL_1 / LL_2",
            "fdi_key": 31
        },
        "lower_l_2": {
            "value": false,
            "index": 2,
            "amount": "",
            "before_step": "",
            "divider": true,
            "use_ipr": false,
            "do_not_perform_ipr": true,
            "tooth_key": "LL_2 / LL_3",
            "fdi_key": 32
        },
        "lower_l_3": {
            "value": false,
            "index": 3,
            "amount": "",
            "before_step": "",
            "divider": true,
            "use_ipr": false,
            "do_not_perform_ipr": true,
            "tooth_key": "LL_3 / LL_4",
            "fdi_key": 33
        },
        "lower_l_4": {
            "value": false,
            "index": 4,
            "amount": "",
            "before_step": "",
            "divider": true,
            "use_ipr": false,
            "do_not_perform_ipr": true,
            "tooth_key": "LL_4 / LL_5",
            "fdi_key": 34
        },
        "lower_l_5": {
            "value": false,
            "index": 5,
            "amount": "",
            "before_step": "",
            "divider": true,
            "use_ipr": false,
            "do_not_perform_ipr": true,
            "tooth_key": "LL_5 / LL_6",
            "fdi_key": 35
        },
        "lower_l_6": {
            "value": false,
            "index": 6,
            "amount": "",
            "before_step": "",
            "divider": true,
            "use_ipr": false,
            "do_not_perform_ipr": true,
            "tooth_key": "LL_6 / LL_7",
            "fdi_key": 36
        },
        "lower_l_7": {
            "value": false,
            "index": 7,
            "amount": "",
            "before_step": "",
            "divider": true,
            "use_ipr": false,
            "do_not_perform_ipr": true,
            "tooth_key": "LL_7 / LL_8",
            "fdi_key": 37
        },
        "lower_l_8": {
            "value": false,
            "index": 8,
            "fdi_key": 38
        }
    },
    "upper_teeth": {
        "upper_r_8": {
            "value": false,
            "index": 8,
            "amount": "",
            "before_step": "",
            "divider": true,
            "use_ipr": false,
            "do_not_perform_ipr": true,
            "fdi_key": 18,
            "tooth_key": "UR_8 / UR_7"
        },
        "upper_r_7": {
            "value": false,
            "index": 7,
            "amount": "",
            "before_step": "",
            "divider": true,
            "use_ipr": false,
            "do_not_perform_ipr": true,
            "fdi_key": 17,
            "tooth_key": "UR_7 / UR_6"
        },
        "upper_r_6": {
            "value": false,
            "index": 6,
            "amount": "",
            "before_step": "",
            "divider": true,
            "use_ipr": false,
            "do_not_perform_ipr": true,
            "fdi_key": 16,
            "tooth_key": "UR_6 / UR_5"
        },
        "upper_r_5": {
            "value": false,
            "index": 5,
            "amount": "",
            "before_step": "",
            "divider": true,
            "use_ipr": false,
            "do_not_perform_ipr": true,
            "fdi_key": 15,
            "tooth_key": "UR_5 / UR_4"
        },
        "upper_r_4": {
            "value": false,
            "index": 4,
            "amount": 0.4,
            "before_step": 5,
            "divider": true,
            "use_ipr": true,
            "do_not_perform_ipr": true,
            "fdi_key": 14,
            "tooth_key": "UR_4 / UR_3"
        },
        "upper_r_3": {
            "value": false,
            "index": 3,
            "amount": "",
            "before_step": "",
            "divider": true,
            "use_ipr": false,
            "do_not_perform_ipr": true,
            "fdi_key": 13,
            "tooth_key": "UR_3 / UR_2"
        },
        "upper_r_2": {
            "value": false,
            "index": 2,
            "amount": "",
            "before_step": "",
            "divider": true,
            "use_ipr": false,
            "do_not_perform_ipr": true,
            "fdi_key": 12,
            "tooth_key": "UR_2 / UR_1"
        },
        "upper_r_1": {
            "value": false,
            "index": 1,
            "amount": 0.6,
            "before_step": 3,
            "divider": true,
            "use_ipr": true,
            "do_not_perform_ipr": true,
            "fdi_key": 11,
            "tooth_key": "UR_1 / UR_0"
        },
        "upper_l_1": {
            "value": false,
            "index": 1,
            "amount": 0.8,
            "before_step": 1,
            "divider": true,
            "use_ipr": true,
            "do_not_perform_ipr": true,
            "tooth_key": "UL_1 / UL_2",
            "fdi_key": 21
        },
        "upper_l_2": {
            "value": false,
            "index": 2,
            "amount": "",
            "before_step": "",
            "divider": true,
            "use_ipr": false,
            "do_not_perform_ipr": true,
            "tooth_key": "UL_2 / UL_3",
            "fdi_key": 22
        },
        "upper_l_3": {
            "value": false,
            "index": 3,
            "amount": "",
            "before_step": "",
            "divider": true,
            "use_ipr": false,
            "do_not_perform_ipr": true,
            "tooth_key": "UL_3 / UL_4",
            "fdi_key": 23
        },
        "upper_l_4": {
            "value": false,
            "index": 4,
            "amount": "",
            "before_step": "",
            "divider": true,
            "use_ipr": false,
            "do_not_perform_ipr": true,
            "tooth_key": "UL_4 / UL_5",
            "fdi_key": 24
        },
        "upper_l_5": {
            "value": false,
            "index": 5,
            "amount": "",
            "before_step": "",
            "divider": true,
            "use_ipr": false,
            "do_not_perform_ipr": false,
            "tooth_key": "UL_5 / UL_6",
            "fdi_key": 25
        },
        "upper_l_6": {
            "value": false,
            "index": 6,
            "amount": "",
            "before_step": "",
            "divider": true,
            "use_ipr": false,
            "do_not_perform_ipr": false,
            "tooth_key": "UL_6 / UL_7",
            "fdi_key": 26
        },
        "upper_l_7": {
            "value": false,
            "index": 7,
            "amount": "",
            "before_step": "",
            "divider": true,
            "use_ipr": false,
            "do_not_perform_ipr": false,
            "tooth_key": "UL_7 / UL_8",
            "fdi_key": 27
        },
        "upper_l_8": {
            "value": false,
            "index": 8,
            "fdi_key": 28
        }
    }
}