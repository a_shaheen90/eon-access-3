import { toast } from "react-toastify";

/*--------------------------------------------------------------------------------*/
/* error Toaster                                                     */
/*--------------------------------------------------------------------------------*/
export function errorToaster(msg) {
  toast.error(msg, {
    position: "bottom-right",
    autoClose: 5000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  });
}
/*--------------------------------------------------------------------------------*/
/* success Toaster                                                     */
/*--------------------------------------------------------------------------------*/
export function successToaster(msg) {
  toast.success(msg, {
    position: "bottom-right",
    autoClose: 5000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  });
}

/*--------------------------------------------------------------------------------*/
/* info Toaster                                                     */
/*--------------------------------------------------------------------------------*/
export function infoToaster(msg) {
  toast.info(msg, {
    position: "bottom-right",
    autoClose: 5000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  });
}
