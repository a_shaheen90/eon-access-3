export function mapIPRTeethToApi(data) {

    let lower_teeth = data.lower_teeth;

    let upper_teeth = data.upper_teeth;
    //remove images from object
    for (var key in lower_teeth) {
        delete lower_teeth[key].active;
        delete lower_teeth[key].not_active;
        delete lower_teeth[key].hover;
        delete lower_teeth[key].image;
    }
    //remove images from object
    for (var key in upper_teeth) {
        delete upper_teeth[key].active;
        delete upper_teeth[key].not_active;
        delete upper_teeth[key].hover;
        delete upper_teeth[key].image;
    }
    return { lower_teeth: lower_teeth, upper_teeth: upper_teeth }
}