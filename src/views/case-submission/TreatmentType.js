import React, { Component } from "react";
import { connect } from "react-redux";

import {
  CardBody,
  CardTitle,
  CardFooter,
  Button,
  Card,
  ModalHeader,
  ModalBody,
  Modal,
  Label,
} from "reactstrap";
import { Formik, Form, ErrorMessage } from "formik";
import { schema, hybridSchema } from "../../helpers/TreatmentTypeValidations";
import hybrid from "../../assets/images/caseSubmission/hybrid.svg";
import aligner from "../../assets/images/caseSubmission/aligner.svg";
import hybrid_active from "../../assets/images/caseSubmission/hybrid_active.svg";
import aligner_active from "../../assets/images/caseSubmission/aligner_active.svg";
import { updateAlignerCase } from "../../api/api";
import { updateAdvancedModules } from "../../redux/CaseSubmission/action";
import { mapTeethToApi } from "../../services/mapTeethToApi";
import { infoToaster } from "../../services/toast";
import { mapAdvancedToApi } from "../../services/mapAdvancedToApi";

const mapStateToProps = (state) => ({
  ...state,
});
const mapDispatchToProps = (dispatch) => ({
  updateAdvancedModules: (payload) => dispatch(updateAdvancedModules(payload)),
});
class TreatmentType extends Component {
  /*--------------------------------------------------------------------------------*/
  /*constructor */
  /*--------------------------------------------------------------------------------*/
  constructor(props) {
    super(props);
    /*init state */
    this.state = {
      type: this.props.caseSubmission?.advanced_modules
        ?.treatment_type?.type || "",
      treatment_type: this.props.caseSubmission?.advanced_modules
        ?.treatment_type?.type || "",
      HybridTreatment: false,
      HybridTooltipOpen: false,
      hybrid_treatment: this.props.caseSubmission?.advanced_modules
        ?.treatment_type?.hybrid_treatment,
      caseId:
        this.props.caseId ||
        (window.location.search.match(/id=([^&]+)/) || [])[1],
      HybridTreatmentType: this.props.caseSubmission?.advanced_modules
        ?.treatment_type?.hybrid_treatment?.type || "",
      aligners: this.props.caseSubmission?.advanced_modules
        ?.treatment_type?.hybrid_treatment?.aligners || "",
      braces: this.props.caseSubmission?.advanced_modules
        ?.treatment_type?.hybrid_treatment?.braces || ""
    };

  }

  checkIfChanged = (oldObj, newObj) => {
    if (oldObj.type === newObj.type) {

      if (oldObj.type === "hybrid" && newObj.type === "hybrid") {
        if (oldObj.hybrid_treatment.type === newObj.hybrid_treatment.type) {

          if (oldObj.hybrid_treatment.aligners === newObj.hybrid_treatment.aligners) {
            if (oldObj.hybrid_treatment.type === "Braces + aligners together" && newObj.hybrid_treatment.type === "Braces + aligners together") {

              if (oldObj.hybrid_treatment.braces === newObj.hybrid_treatment.braces && oldObj.hybrid_treatment.aligners === newObj.hybrid_treatment.aligners) {
                return false
              } else {
                return true
              }
            } else {
              if (oldObj.hybrid_treatment.aligners === newObj.hybrid_treatment.aligners) {
                return false
              } else { return true }
            }
          }
        }
      }
    } else {
      return true
    }


  }
  /*--------------------------------------------------------------------------------*/
  /*Treatment type step submit  */
  /*--------------------------------------------------------------------------------*/
  handleSubmit = (values, { setSubmitting }) => {
    const { hybrid_treatment } = this.state;
    const caseId =
      this.props.caseId ||
      (window.location.search.match(/id=([^&]+)/) || [])[1];
    const { doctorId } = this.props;
    /*--------------------------------------------------------------------------------*/

    let check = this.checkIfChanged(this.props.caseSubmission?.advanced_modules
      ?.treatment_type, {
      type: values.treatment_type,
      ...(values.treatment_type === "hybrid" && {
        hybrid_treatment: hybrid_treatment,
      }),
    })
    /*--------------------------------------------------------------------------------*/

    if (check === false) {
      infoToaster("nothing is changed .......")
      this.props.NextStep();
    } else {
      setSubmitting(true);
      let data = {
        aligner_case: {
          step: "Treatment Type",
          advanced_modules: {
            ...this.props.caseSubmission.advanced_modules,
            ipr: {
              ...this.props.caseSubmission.advanced_modules.ipr,
              iprRestriction: mapTeethToApi(this.props.customeTeethReducer.Case_IPR)
            },
            archExpansion: {
              ...this.props.caseSubmission.advanced_modules.archExpansion,
              teeth: mapTeethToApi(
                this.props.customeTeethReducer.Case_Arch_Expansion
              )
            },
            attachments: {
              ...this.props.caseSubmission.advanced_modules.attachments,
              attachmentsRestriction: mapTeethToApi(
                this.props.customeTeethReducer.Case_Attachments
              )
            },
            elastics: {
              ...this.props.caseSubmission.advanced_modules.elastics,
              teeth: mapTeethToApi(this.props.customeTeethReducer.Case_Elastics),
              buttons_teeth: mapTeethToApi(this.props.customeTeethReducer.Case_Elastics_Buttons),
              cuts_teeth: mapTeethToApi(this.props.customeTeethReducer.Case_Elastics_Cuts),
            },
            extraction: {
              ...this.props.caseSubmission.advanced_modules.extraction,
              teeth: mapTeethToApi(this.props.customeTeethReducer.Case_Extraction),
            },
            overCorrection: {
              ...this.props.caseSubmission.advanced_modules.overCorrection,
              teeth: mapTeethToApi(
                this.props.customeTeethReducer.Case_Overcorrections
              ),
            },
            pontics: {
              ...this.props.caseSubmission.advanced_modules.pontics,
              teeth: mapTeethToApi(this.props.customeTeethReducer.Case_Pontics),
            },
            torqueEnhancers: {
              ...this.props.caseSubmission.advanced_modules.torqueEnhancers,
              teeth: mapTeethToApi(
                this.props.customeTeethReducer.Case_Torque_Enhancers
              ),
            },
            malocclusion: {
              ...this.props.caseSubmission.advanced_modules.malocclusion,
              crossbite: {
                ...this.props.caseSubmission.advanced_modules.malocclusion
                  .crossbite,
                teeth: mapTeethToApi(
                  this.props.customeTeethReducer.Case_Crossbites
                ),
              },
              crowding: {
                ...this.props.caseSubmission.advanced_modules.malocclusion
                  .crowding,
                teeth: mapTeethToApi(
                  this.props.customeTeethReducer.Case_Crowding
                ),
              },
              classII: {
                ...this.props.caseSubmission.advanced_modules.malocclusion
                  .classII,
                teeth: mapTeethToApi(
                  this.props.customeTeethReducer.Case_ClassII
                ),
              },
              classIII: {
                ...this.props.caseSubmission.advanced_modules.malocclusion
                  .classIII,
                teeth: mapTeethToApi(
                  this.props.customeTeethReducer.Case_ClassIII
                ),
              },
            },
            all_modules: mapAdvancedToApi(this.props.advancedModules.modules),
            treatment_type: {
              type: values.treatment_type,
              ...(values.treatment_type === "hybrid" && {
                hybrid_treatment: hybrid_treatment,
              }),
            }
          },
        },
      };



      updateAlignerCase(doctorId, caseId, JSON.stringify(data))
        .then((res) => {
          setSubmitting(false);
          this.props.NextStep();
          this.props.updateAdvancedModules({
            key: "treatment_type",
            data: data.aligner_case.advanced_modules.treatment_type,
          });
        })
        .catch((error) => {
          setSubmitting(false);
        });
    }


  };
  /*--------------------------------------------------------------------------------*/
  /*hybrid treatment submit  */
  /*--------------------------------------------------------------------------------*/
  handleHybridSubmit = (values, { setSubmitting }) => {
    setSubmitting(true);
    this.toggleHybridTreatmentModal();

    this.setState({
      hybrid_treatment: {
        type: values.type,
        aligners: values.aligners,
        ...(values.type === "Braces + aligners together" && {
          braces: values.braces,
        }),
      },
    });
    setSubmitting(false);
  };
  /*--------------------------------------------------------------------------------*/
  /*toggle Hybrid Treatment Modal */
  /*--------------------------------------------------------------------------------*/
  toggleHybridTreatmentModal = () => {
    // if (this.state.Treatment_Type === "hybrid") {
    setTimeout(() => {
      this.setState({ HybridTreatment: !this.state.HybridTreatment });
    }, 50);
    //}
  };
  /*--------------------------------------------------------------------------------*/
  /*toggle Hybrid Treatment info */
  /*--------------------------------------------------------------------------------*/
  toggleHybridTreatmentInfo = () => {
    setTimeout(() => {
      this.setState({ HybridTreatment: !this.state.HybridTreatment, type: "" });
    }, 50)
  };
  render() {
    const { treatment_type } = this.props.caseSubmission?.advanced_modules;
    const { type, HybridTreatmentType, braces, aligners } = this.state
    return (
      <div>
        <CardTitle className="border-bottom pb-3 title_active-gco case-title">
          <div className="step-header-title">
            <span>Treatment Type</span>
            <span className="patient_info">
              <div>{this.props.patient?.patient?.full_name}</div>
              <div> Case {this.state?.caseId}</div>

            </span>
          </div>
        </CardTitle>
        <Formik
          enableReinitialize
          initialValues={{
            treatment_type: type || "",
          }}
          validationSchema={schema}
          onSubmit={this.handleSubmit}
        >
          {({
            touched,
            errors,
            isSubmitting,
            values,
            handleSubmit,
            handleBlur,
            setFieldValue,
          }) => (
            <Form className="form-material" onSubmit={handleSubmit}>
              <div className="step-progress-new-case upload-container">
                {isSubmitting && (
                  <div className={`loading2`}>
                    <div className="spinner_2"></div>
                  </div>
                )}
              </div>
              <CardBody id="case-submission" className="treatment_type_grid">
                <Card
                  className={`p-3 custom_shadow ${values.treatment_type === "aligner"
                    ? "active_impression_card"
                    : "impression_card"
                    }`}
                  onClick={() => {
                    setFieldValue("treatment_type", "aligner");
                    this.setState({ treatment_type: "aligner", type: "aligner" });
                  }}
                  id="treatment_type"
                >
                  <img
                    src={
                      values.treatment_type === "aligner"
                        ? aligner_active
                        : aligner
                    }
                    className="treatment-type-img"
                    alt="Aligner Treatment"
                  />
                  <CardTitle className="pt-2 m-0 treatment-type-title">
                    Aligner Treatment
                  </CardTitle>
                </Card>

                {
                  this.props.gco_doctor && <Card
                    className={`p-3 custom_shadow ${values.treatment_type === "hybrid"
                      ? "active_impression_card"
                      : "impression_card"
                      }`}
                    onClick={() => {
                      setFieldValue("treatment_type", "hybrid");
                      this.setState({ treatment_type: "hybrid", type: "hybrid" });
                      this.toggleHybridTreatmentModal();
                    }}
                    id="treatment_type"
                  >
                    <img
                      src={
                        values.treatment_type === "hybrid"
                          ? hybrid_active
                          : hybrid
                      }
                      className="treatment-type-img"
                      alt="Hybrid Treatment"
                    />
                    <CardTitle className="pt-2 m-0 treatment-type-title ">
                      Hybrid Treatment
                    <span
                        className="icon-container tooltip1"
                        id="HybridTreatmentMoreInfo"
                      >
                        <i className="fas fa-exclamation-circle "></i>
                        <div
                          className={`${window.innerWidth < 840
                            ? "tooltip-bottom"
                            : "tooltip1-right"
                            }  custom_shadow`}
                        >
                          <div className="tooltip-title">
                            When to consider hybrid treatment:
                        </div>
                          <div className="tooltip-body">
                            Hybrid treatment clear aligners therapy with fixed
                            braces for treating malocclusion when the magnitude of
                            some movements would require a higher number of
                            aligners or if the objectives of the treatment are
                            difficult to be achieved with aligners treatment
                            alone.
                        </div>
                          <i></i>
                        </div>
                      </span>
                    </CardTitle>
                  </Card>

                }  </CardBody>
              <ErrorMessage
                component="div"
                name={"treatment_type"}
                className="custom-invalid-feedback m-1 text-center"
              />
              <CardFooter className="p-3">
                {/* <Button
                  className={`btn custom_shadow ${"preference-btn"} float-left text-center  btn-radius`}
                  size="sm"
                  type="button"
                  disabled={isSubmitting}
                  onClick={() => this.props.PrevStep()}
                >
                  Patient Information
                </Button> */}
                <Button
                  className={`btn custom_shadow ${"preference-btn"} float-right text-center mb-2 btn-radius`}
                  size="sm"
                  type="submit"
                  disabled={isSubmitting}
                >
                  Photos + X-rays   <i className="fas fa-angle-right arrows-icon pl-1"></i>
                </Button>
              </CardFooter>
            </Form>
          )}
        </Formik>

        {/*--------------------------------------------------------------------------------*/}
        {/*Hybrid Treatment Modal */}
        {/*--------------------------------------------------------------------------------*/}
        <Modal
          isOpen={this.state.HybridTreatment}
          toggle={this.toggleHybridTreatmentInfo}
          className="modal-top-20 modal-width-60"
        >
          <ModalHeader
            className="modal-title"
            toggle={this.toggleHybridTreatmentInfo}
          >
            Hybrid Treatment
          </ModalHeader>

          <ModalBody className="p-0">
            <Formik
              enableReinitialize
              initialValues={{
                type: HybridTreatmentType,
                aligners: aligners,

                braces: braces,
              }}
              validationSchema={hybridSchema}
              onSubmit={this.handleHybridSubmit}
            >
              {({
                touched,
                errors,
                isSubmitting,
                values,
                handleSubmit,
                handleBlur,
                setFieldValue,
              }) => (
                <Form className="form-material" onSubmit={handleSubmit}>
                  <CardBody>
                    <div className="braces-type-grid">
                      <Button
                        className={`btn   ${values.type === "Braces before aligners"
                          ? "active-btn"
                          : "inactive-btn"
                          } text-center h-100  btn-radius`}
                        size="sm"
                        name="type"
                        onClick={() => {
                          setFieldValue("type", "Braces before aligners");
                          setFieldValue("aligners", "");
                          setFieldValue("braces", "");
                          this.setState({ HybridTreatmentType: "Braces before aligners", braces: "", aligners: "" })
                        }}
                      >
                        Braces before aligners
                      </Button>
                      <Button
                        className={`btn   ${values.type === "Braces + aligners together"
                          ? "active-btn"
                          : "inactive-btn"
                          } text-center h-100 btn-radius`}
                        size="sm"
                        name="type"
                        onClick={() => {
                          setFieldValue("type", "Braces + aligners together");
                          setFieldValue("aligners", "");
                          setFieldValue("braces", "");
                          this.setState({ HybridTreatmentType: "Braces + aligners together", braces: "", aligners: "" })

                        }}
                      >
                        Braces + aligners together
                      </Button>
                    </div>

                    <ErrorMessage
                      component="div"
                      name={"type"}
                      className="custom-invalid-feedback m-1 text-center"
                    />
                    {values.type === "Braces + aligners together" && (
                      <div>
                        <div className="aligner-braces-grid mt-5">
                          <Label className="title_active-gco ">Aligners</Label>

                          <Button
                            className={`btn w-100  ${values.aligners === "upper"
                              ? "active-btn"
                              : "inactive-btn"
                              } text-center  btn-radius`}
                            size="sm"
                            onClick={() => {
                              setFieldValue("aligners", "upper");
                              setFieldValue("braces", "lower");
                              this.setState({ braces: "lower", aligners: "upper" })

                            }}
                            name="aligners"
                          >
                            Upper
                          </Button>
                          <Button
                            className={`btn w-100  ${values.aligners === "lower"
                              ? "active-btn"
                              : "inactive-btn"
                              } text-center  btn-radius`}
                            size="sm"
                            onClick={() => {
                              setFieldValue("aligners", "lower");
                              setFieldValue("braces", "upper");
                              this.setState({ braces: "upper", aligners: "lower" })

                            }}
                            name="aligners"
                          >
                            Lower
                          </Button>
                        </div>
                        <ErrorMessage
                          component="div"
                          name={"aligners"}
                          className="custom-invalid-feedback m-1 text-center"
                        />
                        <div className="aligner-braces-grid mt-2">
                          <Label className="title_active-gco ">Braces</Label>

                          <Button
                            className={`btn w-100  ${values.braces === "upper"
                              ? "active-btn"
                              : "inactive-btn"
                              } text-center  btn-radius`}
                            size="sm"
                            onClick={() => {
                              setFieldValue("braces", "upper");
                              setFieldValue("aligners", "lower");
                              this.setState({ braces: "upper", aligners: "lower" })

                            }}
                            name="braces"
                          >
                            Upper
                          </Button>
                          <Button
                            className={`btn w-100  ${values.braces === "lower"
                              ? "active-btn"
                              : "inactive-btn"
                              } text-center  btn-radius`}
                            size="sm"
                            onClick={() => {
                              setFieldValue("braces", "lower");
                              setFieldValue("aligners", "upper");
                              this.setState({ braces: "lower", aligners: "upper" })

                            }}
                            name="braces"
                          >
                            Lower
                          </Button>
                        </div>
                        <ErrorMessage
                          component="div"
                          name={"aligners"}
                          className="custom-invalid-feedback m-1 text-center"
                        />
                      </div>
                    )}

                    {values.type === "Braces before aligners" && (
                      <div>
                        <div className="aligners-grid mt-5">
                          <Label className="title_active-gco ">Aligners</Label>

                          <Button
                            className={`btn w-100  ${values.aligners === "upper"
                              ? "active-btn"
                              : "inactive-btn"
                              } text-center  btn-radius`}
                            size="sm"
                            onClick={() => {
                              setFieldValue("aligners", "upper");
                              this.setState({ aligners: "upper" })

                            }}
                            name="aligners"
                          >
                            Upper
                          </Button>
                          <Button
                            className={`btn w-100  ${values.aligners === "lower"
                              ? "active-btn"
                              : "inactive-btn"
                              } text-center  btn-radius`}
                            size="sm"
                            onClick={() => {
                              setFieldValue("aligners", "lower");
                              this.setState({ aligners: "lower" })

                            }}
                            name="aligners"
                          >
                            Lower
                          </Button>
                          <Button
                            className={`btn w-100  ${values.aligners === "both"
                              ? "active-btn"
                              : "inactive-btn"
                              } text-center  btn-radius`}
                            size="sm"
                            onClick={() => {
                              setFieldValue("aligners", "both");
                              this.setState({ aligners: "both" })

                            }}
                            name="aligners"
                          >
                            Both
                          </Button>
                        </div>
                        <ErrorMessage
                          component="div"
                          name={"aligners"}
                          className="custom-invalid-feedback m-1 text-center"
                        />
                      </div>
                    )}
                  </CardBody>
                  <CardFooter className="p-3">
                    <Button
                      className={`btn mb-3 ${"preference-btn"} float-right text-center  btn-radius`}
                      size="sm"
                      type="submit"
                      disabled={isSubmitting}
                    >
                      save
                    </Button>
                  </CardFooter>
                </Form>
              )}
            </Formik>
          </ModalBody>
        </Modal>
        {/*--------------------------------------------------------------------------------*/}
        {/* end Hybrid Treatment Modal */}
        {/*--------------------------------------------------------------------------------*/}
      </div>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(TreatmentType);
