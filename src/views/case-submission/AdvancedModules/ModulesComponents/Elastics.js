import React, { Component } from "react";
import { connect } from "react-redux";

import { Button, Row, Col, Input } from "reactstrap";
import ToggleSwitch from "../../../../components/Switch/Switch";
import CustomeTeeth from "../../../Teeth/CustomeTeeth";
import { updateAdvancedModules } from "../../../../redux/CaseSubmission/action";
import { mapTeethToApi } from "../../../../services/mapTeethToApi";
import { Clear_Teeth_Data } from "../../../../redux/customeTeeth/action"
import TreatmentOptions from "../../../../components/TreatmentOptions/TreatmentOptions";

const mapStateToProps = (state) => ({
  ...state,
});
const mapDispatchToProps = (dispatch) => ({
  updateAdvancedModules: (payload) => dispatch(updateAdvancedModules(payload)),
  Clear_Teeth_Data: (payload) => dispatch(Clear_Teeth_Data(payload)),

});
class Elastics extends Component {
  /* --------------------------------------------------------------------------------*/
  /* constructor */
  /* --------------------------------------------------------------------------------*/
  constructor(props) {
    super(props);
    /* init state */
    this.state = {
      use_elastics: this.props.caseSubmission.advanced_modules.elastics
        .useElastics,
      buttons:
        this.props.caseSubmission.advanced_modules.elastics.type === "buttons",
      cuts: this.props.caseSubmission.advanced_modules.elastics.type === "cuts",
      type: this.props.caseSubmission.advanced_modules.elastics.type,
      details: this.props.caseSubmission.advanced_modules.elastics.details,
      use_buttons: this.props.caseSubmission.advanced_modules.elastics.use_buttons,
      use_cuts: this.props.caseSubmission.advanced_modules.elastics.use_cuts,
    };
  }
  UpdateAdvancedModules = () => {
    this.props.updateAdvancedModules({
      key: "elastics",
      data: {
        useElastics: this.state.use_elastics,
        teeth: this.props.customeTeethReducer.Case_Elastics,
        buttons_teeth: this.props.customeTeethReducer.Case_Elastics_Buttons,
        cuts_teeth: this.props.customeTeethReducer.Case_Elastics_Cuts,
        details: this.state.details,
        type: this.state.type,
        use_buttons: this.state.use_buttons,
        use_cuts: this.state.use_cuts,
      },
    });
    this.props.save();
  };
  /* --------------------------------------------------------------------------------*/
  /* handel Treatment Options Changes */
  /* --------------------------------------------------------------------------------*/
  handelTreatmentOptionsChanges = (value, key) => {
    this.setState({
      [key]: value
    })
  }


  render() {
    const { use_elastics, buttons, cuts, use_buttons, use_cuts, details } = this.state;

    return (
      <div>
        <div className="form-group content form-block-holder grid_container ">

          <div className="text-center">
            <div className="control-label title_active-gco text-center mb-2">
              Use elastics for this case{" "}
            </div>

            <TreatmentOptions value={use_elastics} name="use_elastics" summary={this.props.summary} onChange={(value) => {
              this.handelTreatmentOptionsChanges(value, "use_elastics")
              if (value !== true) {
                this.setState({
                  type: "",
                  details: "",
                  use_buttons: false,
                  use_cuts: false
                })
                this.props.Clear_Teeth_Data("Case_Elastics")
                this.props.Clear_Teeth_Data("Case_Elastics_Buttons")
                this.props.Clear_Teeth_Data("Case_Elastics_Cuts")
              }
            }} />
          </div>
          <div>
            <Button
              onClick={() => {
                this.setState({
                  buttons: !buttons,
                  type: "buttons",
                  use_buttons: !use_buttons
                });

                buttons &&
                  this.props.Clear_Teeth_Data("Case_Elastics_Buttons")

              }}
              className={`btn btn-radius text-center w-8 custom_shadow ${use_buttons
                ? "active_discrepancy-btn"
                : "discrepancy-btn"
                } `}
              disabled={!use_elastics || use_elastics === "eonToDecide"}
              style={{ pointerEvents: this.props.summary ? "none" : "" }}

            >
              Buttons
            </Button>
            <div>
              <div className="control-label title_active-gco text-center pb-2 mt-2 ">
                Placement
              </div><div className="teeth_selector">
                <CustomeTeeth action_key="Case_Elastics_Buttons" disabled={!use_elastics || use_elastics === "eonToDecide" || !use_buttons}

                  summary={this.props.summary} />
              </div>

            </div>

            <Button
              onClick={() => {
                this.setState({
                  cuts: !cuts,
                  type: "cuts",
                  use_cuts: !use_cuts
                });
                cuts && this.props.Clear_Teeth_Data("Case_Elastics_Cuts")

              }}
              className={`btn btn-radius text-center w-8 custom_shadow ${use_cuts ? "active_discrepancy-btn" : "discrepancy-btn"
                } `}
              disabled={!use_elastics || use_elastics === "eonToDecide"}
              style={{ pointerEvents: this.props.summary ? "none" : "" }}

            >
              Cuts
            </Button>
            <div>
              <div className="control-label title_active-gco text-center pb-2 mt-2 ">
                Placement
              </div><div className="teeth_selector">
                <CustomeTeeth action_key="Case_Elastics_Cuts" disabled={!use_elastics || use_elastics === "eonToDecide" || !use_cuts}
                  cuts={true}
                  summary={this.props.summary} />
              </div>

            </div>
          </div>

          <div>
            {" "}
            <Input
              className="teeth_selector"
              name="elastics_details"
              id="elastics_details"
              type="textarea"
              placeholder="Please add details"
              rows={5}
              value={details}
              onChange={(e) => {
                this.setState({ details: e.target.value });
              }}
              disabled={!use_elastics || use_elastics === "eonToDecide" || this.props.summary}
            />{" "}
          </div>
        </div>

        {
          !this.props.summary && <Button
            className={`btn mt-2 ${"preference-btn"}  float-right text-center  btn-radius`}
            size="sm"
            type="button"
            onClick={() => this.UpdateAdvancedModules()}
          >
            Save
        </Button>
        }
      </div>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Elastics);
