<Formik
  initialValues={{
    front_smiling: "",
    front_pose: "",
    profile: "",
    upper_occlusal: "",
    lower_occlusal: "",
    left_buccal: "",
    frontal: "",
    right_buccal: "",
    panoramic_xray: "",
    cephalometric_xray: "",
  }}
  // validationSchema={schema}
  onSubmit={this.handleSubmit}
>
  {({
    touched,
    errors,
    isSubmitting,
    values,
    handleSubmit,
    handleBlur,
    setFieldValue,
  }) => (
    <Form className="form-material" onSubmit={handleSubmit}>
      <CardBody id="case-submission">
        <div className="photos-xrays-grid">
          <div>
            <div className="inner-photos-grid">
              <div xs="12" md="6" lg="4">
                <Card className="p-2">
                  <CardTitle className="border-bottom pb-2 title_active">
                    Front Smiling
                  </CardTitle>
                  <div className="image-overlay-container">
                    <label name="front_smiling" htmlFor="front_smiling_photo">
                       <CardImg style="filter: grayscale(100%);"
                        src={
                          this.state.front_smiling
                            ? this.state.front_smiling
                            : Photos.front_smiling
                        } 
                      />
                    </label>
                    <input
                      type="file"
                      id="front_smiling_photo"
                      onChange={(e) =>
                        this.FrontSmilingOnUpload(e, "front_smiling_photo")
                      }
                      multiple={false}
                      className="visually-hidden"
                      accept="image/x-png,image/gif,image/jpeg"
                    />
                    {this.state.showProgress && (
                      <div className="overlay-loader">
                        <CircularProgressbar
                          className="circule-progress-bar"
                          value={this.state.loading_percentage}
                          text={`${this.state.loading_percentage}%`}
                        />
                      </div>
                    )}
                  </div>
                </Card>
                <ErrorMessage
                  component="div"
                  name="front_smiling"
                  className="custom-invalid-feedback m-1"
                />
              </div>
              <div xs="12" md="6" lg="4">
                <Card className="p-2">
                  <CardTitle className="border-bottom pb-2 title_active">
                    Front Pose
                  </CardTitle>
                  <CardImg src={Photos.front_pose} name="front_pose" />
                </Card>
                <ErrorMessage
                  component="div"
                  name="front_pose"
                  className="custom-invalid-feedback m-1"
                />
              </div>{" "}
              <div xs="12" md="6" lg="4">
                <Card className="p-2">
                  <CardTitle className="border-bottom pb-2 title_active">
                  Profile
                  </CardTitle>
                  <CardImg src={Photos.profile} name="profile" />
                </Card>
                <ErrorMessage
                  component="div"
                  name="profile"
                  className="custom-invalid-feedback m-1"
                />
              </div>
              <div xs="12" md="6" lg="4">
                <Card className="p-2">
                  <CardTitle className="border-bottom pb-2 title_active">
                    Upper Occlusal
                  </CardTitle>
                  <CardImg src={Photos.front_smiling} name="upper_occlusal" />
                </Card>
                <ErrorMessage
                  component="div"
                  name="upper_occlusal"
                  className="custom-invalid-feedback m-1"
                />
              </div>
              <div xs="12" md="6" lg="4">
                <Card className="p-2">
                  <CardTitle className="border-bottom pb-2 title_active">
                    Frontal
                  </CardTitle>
                  <CardImg src={Photos.front_smiling} name="frontal" />
                </Card>
                <ErrorMessage
                  component="div"
                  name="frontal"
                  className="custom-invalid-feedback m-1"
                />
              </div>
              <div xs="12" md="6" lg="4">
                <Card className="p-2">
                  <CardTitle className="border-bottom pb-2 title_active">
                    Lower Occlusal
                  </CardTitle>
                  <CardImg src={Photos.front_smiling} name="lower_occlusal" />
                </Card>
                <ErrorMessage
                  component="div"
                  name="lower_occlusal"
                  className="custom-invalid-feedback m-1"
                />
              </div>
              <div xs="12" md="6" lg="4">
                <Card className="p-2">
                  <CardTitle className="border-bottom pb-2 title_active">
                    Right Buccal
                  </CardTitle>
                  <CardImg src={Photos.front_smiling} name="right_buccal" />
                </Card>{" "}
                <ErrorMessage
                  component="div"
                  name="right_buccal"
                  className="custom-invalid-feedback m-1"
                />
              </div>
              <div xs="12" md="6" lg="4"></div>
              <div xs="12" md="6" lg="4">
                <Card className="p-2">
                  <CardTitle className="border-bottom pb-2 title_active">
                    Left Buccal
                  </CardTitle>
                  <CardImg src={Photos.front_smiling} name="left_buccal" />
                </Card>
                <ErrorMessage
                  component="div"
                  name="left_buccal"
                  className="custom-invalid-feedback m-1"
                />
              </div>
            </div>
            <div className="border-bottom-2"></div>
          </div>

          <div>
            <div className="inner-xrays-grid">
              <div>
                <Card className="p-2">
                  <CardTitle className="border-bottom pb-2 title_active">
                    Panoramic Xray
                  </CardTitle>
                  <CardImg src={Photos.front_smiling} name="panoramic_xray" />
                </Card>
                <ErrorMessage
                  component="div"
                  name="panoramic_xray"
                  className="custom-invalid-feedback m-1"
                />
              </div>
              <div xs="12" md="12" lg="12">
                <Card className="p-2">
                  <CardTitle className="border-bottom pb-2 title_active">
                    Cephalometric xray
                  </CardTitle>
                  <CardImg
                    src={Photos.front_smiling}
                    name="cephalometric_xray"
                  />
                </Card>
                <ErrorMessage
                  component="div"
                  name="cephalometric_xray"
                  className="custom-invalid-feedback m-1"
                />
              </div>
              <div xs="12" md="12" lg="12"></div>
            </div>
          </div>
        </div>
      </CardBody>
      <CardFooter className="p-3">
        <Button
          className={`btn  ${"preference-btn"} float-left text-center  btn-radius`}
          size="sm"
          type="button"
          disabled={isSubmitting}
          onClick={() => this.props.PrevStep()}
        >
          Patient Information
        </Button>{" "}
        <Button
          className={`btn  ${"preference-btn"} float-right text-center  btn-radius`}
          size="sm"
          type="submit"
          disabled={isSubmitting}
        >
          Impressions
        </Button>{" "}
      </CardFooter>{" "}
    </Form>
  )}
</Formik>;
