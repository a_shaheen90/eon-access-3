import React from "react";
import { connect } from "react-redux";
import {
  Row,
  Col,
  Card,
  CardBody,
  CardTitle,
  Button,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Progress,
  Form,
  FormGroup,
  Label,
  Input,
  Modal,
  ModalHeader,
  ModalBody,
} from "reactstrap";
import ReactTable from "react-table";

import classnames from "classnames";

import img1 from "../../assets/images/users/7.jpg";
import img2 from "../../assets/images/users/3.jpg";
import img3 from "../../assets/images/users/4.jpg";
import img4 from "../../assets/images/users/5.jpg";

import time1 from "../../assets/images/big/img1.jpg";
import time2 from "../../assets/images/big/img2.jpg";
import time3 from "../../assets/images/big/img3.jpg";
import time4 from "../../assets/images/big/img4.jpg";
import {
  getDoctorInfo,
  getDoctorAddresses,
  UpdateDoctorInfo,
  updateAddress,
  createAddress,
  deleteAddress,
} from "../../api/api";
import { setUserInfo } from "../../redux/user/action";
import { successToaster } from "../../services/toast";
import { uploadFile, keyToUrl } from "../../helpers/s3";
import {
  countries,
  GetCountryLabel,
  GetCountryValue,
} from "../../helpers/countries";
import Select from "react-select";

const mapStateToProps = (state) => ({
  ...state,
});
const mapDispatchToProps = (dispatch) => ({
  setUserInfo: (payload) => dispatch(setUserInfo(payload)),
});
class Profile extends React.Component {
  //Tabs
  constructor(props) {
    super(props);
    this.state = {
      activeTab: this.props.history?.location?.state
        ? this.props.history.location?.state?.activeTab
        : "3",
      id: props.userReducer.currentUser.id,
      doctor: {},
      addresses: [],
      address: {},
      modal: false,
      modal2: false,
      currentUser: props.userReducer.currentUser,
      obj: {},
      subscribe: false,
      doctor_image_value: "",
      doctor_image: false,
      doctor_photo: "",
      country: "",
      city: "",
    };
    // console.log(this.props.history.location.state.activeTab, "profile props ");
  }
  componentDidMount() {
    this.GetDoctorInfo();
    this.getDoctorAddresses();
  }
  GetDoctorInfo = () => {
    getDoctorInfo(this.props.userReducer.currentUser.id).then((res) => {
      this.setState({
        doctor: res,
        subscribe: res.subscribe,
        doctor_image_value: "",
        doctor_image: false,
        doctor_photo: "",
      });
    });
  };
  getDoctorAddresses = () => {
    getDoctorAddresses(this.props.userReducer.currentUser.id).then((res) => {
      this.setState({ addresses: res, address: res[0] });
    });
  };
  toggle = (tab) => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  };
  toggle2 = () => {
    this.setState({
      modal: !this.state.modal,
    });
  };

  toggle3 = () => {
    this.setState({
      modal2: !this.state.modal2,
    });
  };
  handleSubmit = (event) => {
    event.preventDefault();
    const data = {
      doctor: {
        first_name: event.target.first_name.value,
        middle_name: event.target.middle_name.value,
        last_name: event.target.last_name.value,
        phone: event.target.phone.value,
        subscribe: this.state.subscribe,
        avatar: this.state.doctor.avatar,
      },
    };

    // localStorage.setItem("currentUser", JSON.stringify(currentUser));
    this.UpdateUser(data);
  };

  handleSubmit2 = (event) => {
    event.preventDefault();
    const data = {
      address: {
        country: this.state.country,
        city: event.target.city.value,
        state: event.target.state.value,
        line1: event.target.line1.value,
        line2: event.target.line2.value,
        zip: event.target.zip.value,
        phone: event.target.phone.value,
      },
    };
    this.setState({ city: event.target.city.value });
    if (event.target.id.value) {
      //update address
      updateAddress(
        this.state.id,
        event.target.id.value,
        JSON.stringify(data)
      ).then((res) => {
        this.getDoctorAddresses();
        this.setState({
          modal: !this.state.modal,
          country: "",
        });
        successToaster("Updated address successfully ");
      });
    } else {
      // add new address
      createAddress(this.state.id, JSON.stringify(data)).then((res) => {
        this.getDoctorAddresses();
        this.setState({
          modal: !this.state.modal,
          country: "",
        });
        successToaster("Created new address successfully");
      });
    }
  };

  uploadImage = async (image, id) => {
    let extention = image.name.split(".");
    return uploadFile({
      name: `doctor-photo/${id}/${new Date().valueOf()}.${
        image.name.split(".")[extention.length - 1]
      }`,
      contentType: image.type,
      file: image,
    });
  };
  //upload front smiling photo
  ProfilePhotoOnUpload = async (e, id) => {
    this.setState({ loading1: true });
    if (e.target.files[0]) {
      let reader = new FileReader();
      reader.addEventListener("load", (evt) => {
        this.setState({
          doctor_photo: evt.currentTarget.result,
        });
      });
      reader.readAsDataURL(e.target.files[0]);

      const file = e.target ? e.target.files[0] : e.files[0];
      const { key } = await this.uploadImage(file, id);

      this.setState({
        doctor_image_value: keyToUrl(key),
        doctor_image: false,
        loading1: false,
      });

      const data = {
        doctor: {
          first_name: this.state.doctor.first_name,
          middle_name: this.state.doctor.middle_name,
          last_name: this.state.doctor.last_name,
          phone: this.state.doctor.phone,
          subscribe: this.state.subscribe,
          avatar: keyToUrl(key),
        },
      };

      this.UpdateUser(data);
    }
  };

  UpdateUser = (data) => {
    const currentUser = {
      avatar: data.doctor.avatar,
      email: this.state.currentUser.email,
      first_name: data.doctor.first_name,
      full_name: `${data.doctor.first_name} ${data.doctor.middle_name} ${data.doctor.last_name}`,
      id: this.state.currentUser.id,
      last_name: data.doctor.last_name,
    };
    UpdateDoctorInfo(this.state.id, JSON.stringify(data))
      .then((res) => {
        this.GetDoctorInfo();
        this.props.setUserInfo(currentUser);
        localStorage.setItem("currentUser", JSON.stringify(currentUser));
        successToaster("updated user info successfully");
      })
      .catch((error) => {
        this.GetDoctorInfo();
      });
  };
  render() {
    const data = this.state.addresses.map((prop, key) => {
      return {
        id: prop.id,
        city: prop.city,
        country: GetCountryLabel(prop.country),
        line1: prop.line1,
        line2: prop.line2,
        state: prop.state,
        zip: prop.zip,
        phone: prop.phone,
        actions: (
          // we've added some custom button actions
          <div className="text-center">
            <style>
              {`
              .fa-trash-alt{
                color:red
              }
              `}
            </style>
            {/* use this button to add a edit kind of action */}
            <Button
              onClick={() => {
                let obj = data.find((o) => o.id === prop.id);
                // console.log(obj, "obj......");
                this.setState({
                  modal: !this.state.modal,
                  obj: obj,
                  country: GetCountryValue(obj.country),
                  city: obj.city,
                });
              }}
              // className="m-1"
              color="white"
              size="md"
              round="true"
              icon="true"
            >
              <i className="far fa-edit" style={{ color: "blue" }} />
            </Button>
            <Button
              onClick={() => {
                let obj = data.find((o) => o.id === prop.id);
                this.setState({
                  modal2: !this.state.modal2,
                  obj: obj,
                });
              }}
              // className="m-1"
              color="white"
              size="md"
              round="true"
              icon="true"
            >
              <i className="fas fa-trash-alt " />
            </Button>
          </div>
        ),
      };
    });

    return (
      <div>
        <Row>
          <Col xs="12" md="12" lg="4">
            <Card>
              <CardBody>
                <div className="text-center mt-4">
                  <div className="buttons-container2 ">
                    <input
                      type="file"
                      id="doctor_photo"
                      onChange={(e) =>
                        this.ProfilePhotoOnUpload(e, "doctor_photo")
                      }
                      multiple={false}
                      className="visually-hidden"
                      accept="image/x-png,image/gif,image/jpeg"
                    />
                    {
                      <div className="upload-container m-auto">
                        {
                          <label htmlFor="doctor_photo" className="upload">
                            <img
                              className="rounded-circle"
                              width="150"
                              src={
                                this.state.doctor_photo
                                  ? this.state.doctor_photo
                                  : this.state.doctor.avatar
                                  ? this.state.doctor.avatar.match(
                                      /http([^&]+)/
                                    )
                                    ? this.state.doctor.avatar
                                    : "https://muratselek.com.tr/wp-content/uploads/2019/01/yorum-icon-avatar-men-768x918.png"
                                  : "https://muratselek.com.tr/wp-content/uploads/2019/01/yorum-icon-avatar-men-768x918.png"
                              }
                              alt={`${this.state.doctor.full_name}`}
                            />

                            {this.state.loading1 && (
                              <center>
                                <div className={` ${"loading"}`}></div>
                              </center>
                            )}
                          </label>
                        }
                      </div>
                    }
                  </div>

                  {/* <img
                    src={this.state.doctor.avatar}
                    className="rounded-circle"
                    width="150"
                    alt=""
                  /> */}
                  <CardTitle className="mt-2">
                    {this.state.doctor.full_name}
                  </CardTitle>
                  {/* <CardSubtitle>{this.state.doctor.email}</CardSubtitle> */}
                  {/* <Row className="text-center justify-content-md-center">
                    <Col xs="4">
                      <a href="/" className="link">
                        <i className="icon-people"></i>
                        <span className="font-medium ml-2">254</span>
                      </a>
                    </Col>
                    <Col xs="4">
                      <a href="/" className="link">
                        <i className="icon-picture"></i>
                        <span className="font-medium ml-2">54</span>
                      </a>
                    </Col>
                  </Row> */}
                </div>
              </CardBody>
              <CardBody className="border-top">
                <div>
                  <small className="text-muted">Email address </small>
                  <h6>{this.state.doctor.email}</h6>
                  <small className="text-muted pt-4 db">Phone</small>
                  <h6>{this.state.doctor.phone}</h6>
                  <small className="text-muted pt-4 db">Address</small>
                  <h6>{`  ${
                    this.state.address
                      ? GetCountryLabel(this.state.address.country)
                      : ""
                  } - ${
                    this.state.address ? this.state.address.city : ""
                  } `}</h6>
                  {/* <div>
                    <Iframe
                      className="position-relative"
                      url="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d470029.1604841957!2d72.29955005258641!3d23.019996818380896!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x395e848aba5bd449%3A0x4fcedd11614f6516!2sAhmedabad%2C+Gujarat!5e0!3m2!1sen!2sin!4v1493204785508"
                      width="280"
                      height="150"
                      frameborder="0"
                      allowfullscreen
                    />
                  </div> */}
                  {/* <small className="text-muted pt-4 db">Social Profile</small>
                  <br />
                  <Button className="btn-circle" color="info">
                    <i className="fab fa-facebook-f"></i>
                  </Button>{" "}
                  <Button className="btn-circle" color="success">
                    <i className="fab fa-twitter"></i>
                  </Button>{" "}
                  <Button className="btn-circle" color="danger">
                    <i className="fab fa-youtube"></i>
                  </Button> */}
                </div>
              </CardBody>
            </Card>
          </Col>
          <Col xs="12" md="12" lg="8">
            <Card>
              <Nav tabs>
                {/* <NavItem>
                  <NavLink
                    className={classnames({
                      active: this.state.activeTab === "1",
                    })}
                    onClick={() => {
                      this.toggle("1");
                    }}
                  >
                    Timeline
                  </NavLink>
                </NavItem> */}
                {/* <NavItem>
                  <NavLink
                    className={classnames({
                      active: this.state.activeTab === "2",
                    })}
                    onClick={() => {
                      this.toggle("2");
                    }}
                  >
                    Profile
                  </NavLink>
                </NavItem> */}
                <NavItem>
                  <NavLink
                    className={classnames({
                      active: this.state.activeTab === "3",
                    })}
                    onClick={() => {
                      this.toggle("3");
                    }}
                  >
                    Setting
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({
                      active: this.state.activeTab === "4",
                    })}
                    onClick={() => {
                      this.toggle("4");
                    }}
                  >
                    Address
                  </NavLink>
                </NavItem>
              </Nav>
              <TabContent activeTab={this.state.activeTab}>
                <TabPane tabId="1">
                  <Row>
                    <Col sm="12">
                      <Card>
                        <CardBody>
                          <div className="steamline mt-0">
                            <div className="sl-item">
                              <div className="sl-left">
                                {" "}
                                <img
                                  src={img2}
                                  alt="user"
                                  className="rounded-circle"
                                />{" "}
                              </div>
                              <div className="sl-right">
                                <div>
                                  <a href="/" className="link">
                                    John Doe
                                  </a>
                                  <span className="sl-date">5 minutes ago</span>
                                  <p>
                                    assign a new task{" "}
                                    <a href="/"> Design weblayout</a>
                                  </p>
                                  <Row>
                                    <Col lg="3" md="6" className="mb-3">
                                      <img
                                        src={time1}
                                        className="img-fluid rounded"
                                        alt=""
                                      />
                                    </Col>
                                    <Col lg="3" md="6" className="mb-3">
                                      <img
                                        src={time2}
                                        className="img-fluid rounded"
                                        alt=""
                                      />
                                    </Col>
                                    <Col lg="3" md="6" className="mb-3">
                                      <img
                                        src={time3}
                                        className="img-fluid rounded"
                                        alt=""
                                      />
                                    </Col>
                                    <Col lg="3" md="6" className="mb-3">
                                      <img
                                        src={time4}
                                        className="img-fluid rounded"
                                        alt=""
                                      />
                                    </Col>
                                  </Row>
                                  <div className="desc">
                                    <a href="/" className="link mr-2">
                                      2 comment
                                    </a>
                                    <a href="/" className="link mr-2">
                                      <i className="fa fa-heart text-danger"></i>{" "}
                                      5 Love
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="sl-item">
                              <div className="sl-left">
                                <img
                                  src={img3}
                                  alt="user"
                                  className="rounded-circle"
                                />
                              </div>
                              <div className="sl-right">
                                <div>
                                  <a href="/" className="link">
                                    John Doe
                                  </a>
                                  <span className="sl-date">5 minutes ago</span>
                                  <Row className="mt-3">
                                    <Col md="3" xs="12">
                                      <img
                                        src={time1}
                                        alt="user"
                                        className="img-fluid rounded"
                                      />
                                    </Col>
                                    <Col md="9" xs="12">
                                      <p>
                                        {" "}
                                        Lorem ipsum dolor sit amet, consectetur
                                        adipiscing elit. Integer nec odio.
                                        Praesent libero. Sed cursus ante dapibus
                                        diam.{" "}
                                      </p>
                                      <a href="/" className="btn btn-success">
                                        {" "}
                                        Design weblayout
                                      </a>
                                    </Col>
                                  </Row>
                                  <div className="desc mt-3">
                                    <a href="/" className="link mr-2">
                                      2 comment
                                    </a>
                                    <a href="/" className="link mr-2">
                                      <i className="fa fa-heart text-danger"></i>{" "}
                                      5 Love
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="sl-item">
                              <div className="sl-left">
                                <img
                                  src={img4}
                                  alt="user"
                                  className="rounded-circle"
                                />
                              </div>
                              <div className="sl-right">
                                <div>
                                  <a href="/" className="link">
                                    John Doe
                                  </a>
                                  <span className="sl-date">5 minutes ago</span>
                                  <p className="mt-2">
                                    {" "}
                                    Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit. Integer nec odio. Praesent
                                    libero. Sed cursus ante dapibus diam. Sed
                                    nisi. Nulla quis sem at nibh elementum
                                    imperdiet. Duis sagittis ipsum. Praesent
                                    mauris. Fusce nec tellus sed augue semper{" "}
                                  </p>
                                </div>
                                <div className="desc mt-3">
                                  <a href="/" className="link mr-2">
                                    2 comment
                                  </a>
                                  <a href="/" className="link mr-2">
                                    <i className="fa fa-heart text-danger"></i>{" "}
                                    5 Love
                                  </a>
                                </div>
                              </div>
                            </div>
                            <div className="sl-item">
                              <div className="sl-left">
                                <img
                                  src={img1}
                                  alt="user"
                                  className="rounded-circle"
                                />
                              </div>
                              <div className="sl-right">
                                <div>
                                  <a href="/" className="link">
                                    John Doe
                                  </a>
                                  <span className="sl-date">5 minutes ago</span>
                                  <blockquote className="mt-2">
                                    Lorem ipsum dolor sit amet, consectetur
                                    adipisicing elit, sed do eiusmod tempor
                                    incididunt
                                  </blockquote>
                                </div>
                              </div>
                            </div>
                          </div>
                        </CardBody>
                      </Card>
                    </Col>
                  </Row>
                </TabPane>
                <TabPane tabId="2">
                  <Row>
                    <Col sm="12">
                      <Card>
                        <CardBody>
                          <Row>
                            <Col md="3" xs="6" className="border-right">
                              <strong>Full Name</strong>
                              <br />
                              <p className="text-muted">
                                {this.state.doctor.full_name}
                              </p>
                            </Col>
                            <Col md="3" xs="6" className="border-right">
                              <strong>Mobile</strong>
                              <br />
                              <p className="text-muted">(123) 456 7890</p>
                            </Col>
                            <Col md="3" xs="6" className="border-right">
                              <strong>Email</strong>
                              <br />
                              <p className="text-muted">
                                {" "}
                                {this.state.doctor.email}
                              </p>
                            </Col>
                            <Col md="3" xs="6" className="border-right">
                              <strong>Location</strong>
                              <br />
                              <p className="text-muted">London</p>
                            </Col>
                          </Row>
                          <p className="mt-4">
                            Donec pede justo, fringilla vel, aliquet nec,
                            vulputate eget, arcu. In enim justo, rhoncus ut,
                            imperdiet a, venenatis vitae, justo. Nullam dictum
                            felis eu pede mollis pretium. Integer tincidunt.Cras
                            dapibus. Vivamus elementum semper nisi. Aenean
                            vulputate eleifend tellus. Aenean leo ligula,
                            porttitor eu, consequat vitae, eleifend ac, enim.
                          </p>
                          <p>
                            Lorem Ipsum is simply dummy text of the printing and
                            typesetting industry. Lorem Ipsum has been the
                            industry&apos;s standard dummy text ever since the
                            1500s, when an unknown printer took a galley of type
                            and scrambled it to make a type specimen book. It
                            has survived not only five centuries{" "}
                          </p>
                          <p>
                            It was popularised in the 1960s with the release of
                            Letraset sheets containing Lorem Ipsum passages, and
                            more recently with desktop publishing software like
                            Aldus PageMaker including versions of Lorem Ipsum.
                          </p>
                          <h4 className="font-medium mt-4">Skill Set</h4>
                          <hr />
                          <h5 className="mt-4">
                            Wordpress <span className="float-right">80%</span>
                          </h5>
                          <Progress value={2 * 5} />
                          <h5 className="mt-4">
                            HTML 5 <span className="float-right">90%</span>
                          </h5>
                          <Progress color="success" value="25" />
                          <h5 className="mt-4">
                            jQuery <span className="float-right">50%</span>
                          </h5>
                          <Progress color="info" value={50} />
                          <h5 className="mt-4">
                            Photoshop <span className="float-right">70%</span>
                          </h5>
                          <Progress color="warning" value={75} />
                        </CardBody>
                      </Card>
                    </Col>
                  </Row>
                </TabPane>
                <TabPane tabId="3">
                  <Row>
                    <Col sm="12">
                      <Card>
                        <CardBody>
                          <Form onSubmit={this.handleSubmit}>
                            <FormGroup>
                              <Label>First Name</Label>
                              <Input
                                type="text"
                                name="first_name"
                                id="first_name"
                                // placeholder={this.state.doctor.first_name}
                                defaultValue={this.state.doctor.first_name}
                              />
                            </FormGroup>
                            <FormGroup>
                              <Label>Middle Name</Label>
                              <Input
                                type="text"
                                name="middle_name"
                                id="middle_name"
                                // placeholder={this.state.doctor.middle_name}
                                defaultValue={this.state.doctor.middle_name}
                              />
                            </FormGroup>
                            <FormGroup>
                              <Label>Last Name</Label>
                              <Input
                                type="text"
                                name="last_name"
                                id="last_name"
                                // placeholder={this.state.doctor.last_name}
                                defaultValue={this.state.doctor.last_name}
                              />
                            </FormGroup>
                            <FormGroup>
                              <Label>Email</Label>
                              <Input
                                type="email"
                                name="email"
                                id="email"
                                readOnly
                                // placeholder={this.state.doctor.email}
                                defaultValue={this.state.doctor.email}
                              />
                            </FormGroup>
                            {/* <FormGroup>
                              <Label>Password</Label>
                              <Input type="password" placeholder="Password" />
                            </FormGroup> */}
                            <FormGroup>
                              <Label>Phone No</Label>
                              <Input
                                type="text"
                                name="phone"
                                id="phone"
                                // placeholder={this.state.doctor.phone}
                                defaultValue={this.state.doctor.phone}
                              />
                            </FormGroup>

                            <FormGroup>
                              <input
                                type="checkbox"
                                name="subscribe"
                                id="subscribe"
                                checked={this.state.subscribe}
                                onChange={() => {
                                  this.setState({
                                    subscribe: !this.state.subscribe,
                                  });
                                }}
                              />
                              <label className="pl-2" htmlFor={`subscribe`}>
                                Subscribe
                              </label>
                            </FormGroup>
                            {/* <FormGroup>
                              <Label>Upload Photo</Label>
                              <Input type="file" name="avatar" id="avatar" />
                            </FormGroup> */}
                            {/* <FormGroup>
                              <Label>Message</Label>
                              <Input type="textarea" />
                            </FormGroup>
                            <FormGroup>
                              <Label>Select Country</Label>
                              <Input type="select">
                                <option>USA</option>
                                <option>India</option>
                                <option>America</option>
                              </Input>
                            </FormGroup> */}
                            <Button
                              className={`btn green-btn-active text-center pull-right `}
                            >
                              Update Profile
                            </Button>
                          </Form>
                        </CardBody>
                      </Card>
                    </Col>
                  </Row>
                </TabPane>

                <TabPane tabId="4">
                  <Row>
                    <Col sm="12">
                      <Card>
                        <CardBody>
                          <Modal
                            isOpen={this.state.modal}
                            toggle={this.toggle2}
                          >
                            <ModalHeader toggle={this.toggle2}>
                              {this.state.obj.country
                                ? `Update Address`
                                : `New Address`}
                            </ModalHeader>
                            <ModalBody>
                              <Form onSubmit={this.handleSubmit2}>
                                <Input
                                  type="hidden"
                                  name="id"
                                  id="id"
                                  defaultValue={this.state.obj.id}
                                />
                                <Row>
                                  <Col>
                                    <FormGroup>
                                      <Label for="country">
                                        Country{" "}
                                        <span style={{ color: "red" }}>*</span>
                                      </Label>
                                      {/* <Input
                                        type="text"
                                        name="country"
                                        id="country"
                                        defaultValue={this.state.obj.country}
                                      /> */}
                                      <Select
                                        defaultValue={{
                                          label: this.state.obj.country,
                                          value: this.state.country,
                                        }}
                                        // value={this.state.country}
                                        onChange={(country) => {
                                          this.setState({
                                            country: country.value,
                                          });
                                        }}
                                        options={countries}
                                      />
                                    </FormGroup>
                                  </Col>
                                  <Col>
                                    <FormGroup>
                                      <Label for="city">
                                        City
                                        <span style={{ color: "red" }}>
                                          *
                                        </span>{" "}
                                      </Label>
                                      <Input
                                        type="text"
                                        name="city"
                                        id="city"
                                        onChange={(e) => {
                                          this.setState({
                                            city: e.target.value,
                                          });
                                        }}
                                        defaultValue={this.state.obj.city}
                                      />
                                    </FormGroup>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col>
                                    <FormGroup>
                                      <Label for="line1">Line 1</Label>
                                      <Input
                                        type="text"
                                        name="line1"
                                        id="line1"
                                        defaultValue={this.state.obj.line1}
                                      />
                                    </FormGroup>
                                  </Col>
                                  <Col>
                                    <FormGroup>
                                      <Label for="line2">Line 2</Label>
                                      <Input
                                        type="text"
                                        name="line2"
                                        id="line2"
                                        defaultValue={this.state.obj.line2}
                                      />
                                    </FormGroup>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col>
                                    <FormGroup>
                                      <Label for="state">State</Label>
                                      <Input
                                        type="text"
                                        name="state"
                                        id="state"
                                        defaultValue={this.state.obj.state}
                                      />
                                    </FormGroup>
                                  </Col>
                                  <Col>
                                    <FormGroup>
                                      <Label for="zip">Zip</Label>
                                      <Input
                                        type="text"
                                        name="zip"
                                        id="zip"
                                        defaultValue={this.state.obj.zip}
                                      />
                                    </FormGroup>
                                  </Col>
                                </Row>
                                <Row>
                                  <Col>
                                    <FormGroup>
                                      <Label for="phone">Phone</Label>
                                      <Input
                                        type="text"
                                        name="phone"
                                        id="phone"
                                        defaultValue={this.state.obj.phone}
                                      />
                                    </FormGroup>
                                  </Col>
                                </Row>
                                <FormGroup>
                                  <Button
                                    className="btn red-btn pull-right m-1 "
                                    size="md"
                                    color="danger"
                                    onClick={this.toggle2}
                                  >
                                    Cancel
                                  </Button>{" "}
                                  <Button
                                    className="btn green-btn pull-right m-1"
                                    size="md"
                                    type="submit"
                                    disabled={
                                      this.state.country === "" ||
                                      this.state.city === ""
                                    }
                                  >
                                    {this.state.obj.country ? `Update` : `Add`}
                                  </Button>
                                </FormGroup>
                              </Form>
                            </ModalBody>
                          </Modal>

                          <Modal
                            isOpen={this.state.modal2}
                            toggle={this.toggle3}
                          >
                            <ModalHeader toggle={this.toggle3}>
                              Delete Address
                            </ModalHeader>
                            <ModalBody>
                              <FormGroup>
                                {" "}
                                <h5>
                                  Are you sure you want to delete this address?
                                </h5>
                              </FormGroup>
                              <FormGroup>
                                <Button
                                  className="btn red-btn pull-right m-1 "
                                  size="md"
                                  color="danger"
                                  onClick={this.toggle3}
                                >
                                  Cancel
                                </Button>
                                <Button
                                  className="btn green-btn pull-right m-1"
                                  size="md"
                                  type="submit"
                                  onClick={() => {
                                    deleteAddress(
                                      this.state.id,
                                      this.state.obj.id
                                    ).then((res) => {
                                      this.getDoctorAddresses();
                                      this.setState({
                                        modal2: !this.state.modal2,
                                      });
                                      successToaster(
                                        "deleted address successfully"
                                      );
                                    });
                                  }}
                                >
                                  Delete
                                </Button>
                              </FormGroup>
                            </ModalBody>
                          </Modal>

                          {/*--------------------------------------------------------------------------------*/}
                          {/* Fixed header-footer table*/}
                          {/*--------------------------------------------------------------------------------*/}
                          {/* <Card>
                            <CardTitle className="mb-0 p-3 border-bottom"> */}
                          <Row className="pb-3">
                            <Col>
                              {" "}
                              <div className="text-right">
                                {/* use this button to add a edit kind of action */}
                                <Button
                                  onClick={() => {
                                    this.setState({
                                      modal: !this.state.modal,
                                      obj: {},
                                    });
                                  }}
                                  className="btn green-btn pull-right"
                                  size="md"
                                  // color="inverse"
                                  // size="md"
                                  // icon="true"
                                >
                                  New Address
                                  {/* <i className="fa fa-plus" /> */}
                                </Button>
                              </div>
                            </Col>
                          </Row>

                          {/* </CardTitle>
                            <CardBody> */}
                          <Row>
                            <Col>
                              <ReactTable
                                data={data}
                                columns={[
                                  {
                                    Header: "Country",
                                    accessor: "country",
                                    width: 130,
                                  },
                                  {
                                    Header: "City",
                                    accessor: "city",
                                    width: 130,
                                  },
                                  // {
                                  //   Header: "Line 1",
                                  //   accessor: "line1",
                                  // },
                                  // {
                                  //   Header: "Line 2",
                                  //   accessor: "line2",
                                  // },
                                  {
                                    Header: "State",
                                    accessor: "state",
                                    width: 130,
                                  },
                                  {
                                    Header: "Zip",
                                    accessor: "zip",
                                    width: 100,
                                  },
                                  {
                                    Header: "Phone",
                                    accessor: "phone",
                                    width: 130,
                                  },
                                  {
                                    Header: "Action",
                                    accessor: "actions",
                                    width: 130,
                                  },
                                ]}
                                defaultPageSize={10}
                                style={{
                                  height: "400px", // This will force the table body to overflow and scroll, since there is not enough room
                                }}
                                className=" -highlight"
                              />
                            </Col>
                          </Row>

                          {/* </CardBody>
                          </Card> */}
                          {/*--------------------------------------------------------------------------------*/}
                          {/* End fixed header-footer table*/}
                          {/*--------------------------------------------------------------------------------*/}
                        </CardBody>
                      </Card>
                    </Col>
                  </Row>
                </TabPane>
              </TabContent>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
