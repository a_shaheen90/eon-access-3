import React, { Component } from "react";
import { PDFDownloadLink } from "@react-pdf/renderer";
import MyDocument from "../../MyDocument";
import { getAlignerCase } from "../../../../api/api";

export default class Summary extends Component {
  /* --------------------------------------------------------------------------------*/
  /* constructor */
  /* --------------------------------------------------------------------------------*/
  constructor(props) {
    super(props);
    /* init state */
    this.state = {
      load: false,
      uid: props.case.uid,
      id: props.id,
      case: props.case,
      caseId:
        props.caseId || (window.location.search.match(/id=([^&]+)/) || [])[1],
    };
  }
  /* --------------------------------------------------------------------------------*/
  /* get aligner case info to create pdf , scroll to top after render  */
  /* --------------------------------------------------------------------------------*/
  componentDidMount() {
    if (!this.state.uid) {
      getAlignerCase(this.state.id, this.state.caseId).then((res) => {
        setTimeout(() => {
          this.setState({ load: true, uid: res.uid, case: res });
        }, 1000);
      });
    } else {
      setTimeout(() => {
        this.setState({ load: true });
      }, 1000);
    }
    window.scrollTo(0, 0);
  }

  /* --------------------------------------------------------------------------------*/
  /* clear state if go out from component   */
  /* --------------------------------------------------------------------------------*/
  componentWillUnmount() {
    // fix Warning: Can't perform a React state update on an unmounted component
    this.setState = (state, callback) => {
      return;
    };
  }

  render() {
    return (
      <div className="mt-5 mb-2">
        <div className="row justify-content-md-center">
          <div className="col">
            <div className="">
              <div className="summary_header">
                {` Thank you for submitting the case ${
                  this.props.caseType === "Retainer" && "(retainer)"
                }, we will send you a
                notification once the case is approved.`}
              </div>

              {this.props.caseType !== "Retainer" && (
                <>
                  <div className="pdf">
                    {/*--------------------------------------------------------------------------------*/}
                    {/* Download Treatment Form (PDF) */}
                    {/*--------------------------------------------------------------------------------*/}
                    {this.state.load && (
                      <PDFDownloadLink
                        document={
                          <MyDocument
                            case={this.state.case}
                            mappedData={this.props.mappedData}
                            doctorInfo={this.props.doctorInfo}
                          />
                        }
                        fileName={`treatment form_${this.state.uid}.pdf`}
                        className="btn green-btn-active text-center pdf-btn"
                      >
                        {({ blob, url, loading, error }) =>
                          loading
                            ? "Loading document..."
                            : "Download Treatment Form (PDF)"
                        }
                      </PDFDownloadLink>
                    )}{" "}
                    {/*--------------------------------------------------------------------------------*/}
                    {/* End Download Treatment Form (PDF) */}
                    {/*--------------------------------------------------------------------------------*/}
                  </div>
                  <div className="p-2">
                    Please download and include printed Treatment Form when
                    shipping impressions
                  </div>
                  <div className=" p-2 term-content-bold">
                    You can now finish the case creation wizard.
                  </div>
                </>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
