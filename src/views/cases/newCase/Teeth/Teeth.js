import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import upper_l_1 from "../../../../assets/images/teeth/not_active/upper_l_1_not_active.png";
import upper_l_2 from "../../../../assets/images/teeth/not_active/upper_l_2_not_active.png";
import upper_l_3 from "../../../../assets/images/teeth/not_active/upper_l_3_not_active.png";
import upper_l_4 from "../../../../assets/images/teeth/not_active/upper_l_4_not_active.png";
import upper_l_5 from "../../../../assets/images/teeth/not_active/upper_l_5_not_active.png";
import upper_l_6 from "../../../../assets/images/teeth/not_active/upper_l_6_not_active.png";
import upper_l_7 from "../../../../assets/images/teeth/not_active/upper_l_7_not_active.png";
import upper_l_8 from "../../../../assets/images/teeth/not_active/upper_l_8_not_active.png";
import upper_r_1 from "../../../../assets/images/teeth/not_active/upper_r_1_not_active.png";
import upper_r_2 from "../../../../assets/images/teeth/not_active/upper_r_2_not_active.png";
import upper_r_3 from "../../../../assets/images/teeth/not_active/upper_r_3_not_active.png";
import upper_r_4 from "../../../../assets/images/teeth/not_active/upper_r_4_not_active.png";
import upper_r_5 from "../../../../assets/images/teeth/not_active/upper_r_5_not_active.png";
import upper_r_6 from "../../../../assets/images/teeth/not_active/upper_r_6_not_active.png";
import upper_r_7 from "../../../../assets/images/teeth/not_active/upper_r_7_not_active.png";
import upper_r_8 from "../../../../assets/images/teeth/not_active/upper_r_8_not_active.png";

import upper_l_1_hover from "../../../../assets/images/teeth/hover/upper_l_1_hover.png";
import upper_l_2_hover from "../../../../assets/images/teeth/hover/upper_l_2_hover.png";
import upper_l_3_hover from "../../../../assets/images/teeth/hover/upper_l_3_hover.png";
import upper_l_4_hover from "../../../../assets/images/teeth/hover/upper_l_4_hover.png";
import upper_l_5_hover from "../../../../assets/images/teeth/hover/upper_l_5_hover.png";
import upper_l_6_hover from "../../../../assets/images/teeth/hover/upper_l_6_hover.png";
import upper_l_7_hover from "../../../../assets/images/teeth/hover/upper_l_7_hover.png";
import upper_l_8_hover from "../../../../assets/images/teeth/hover/upper_l_8_hover.png";
import upper_r_1_hover from "../../../../assets/images/teeth/hover/upper_r_1_hover.png";
import upper_r_2_hover from "../../../../assets/images/teeth/hover/upper_r_2_hover.png";
import upper_r_3_hover from "../../../../assets/images/teeth/hover/upper_r_3_hover.png";
import upper_r_4_hover from "../../../../assets/images/teeth/hover/upper_r_4_hover.png";
import upper_r_5_hover from "../../../../assets/images/teeth/hover/upper_r_5_hover.png";
import upper_r_6_hover from "../../../../assets/images/teeth/hover/upper_r_6_hover.png";
import upper_r_7_hover from "../../../../assets/images/teeth/hover/upper_r_7_hover.png";
import upper_r_8_hover from "../../../../assets/images/teeth/hover/upper_r_8_hover.png";

import upper_l_1_active from "../../../../assets/images/teeth/active/upper_l_1_active.png";
import upper_l_2_active from "../../../../assets/images/teeth/active/upper_l_2_active.png";
import upper_l_3_active from "../../../../assets/images/teeth/active/upper_l_3_active.png";
import upper_l_4_active from "../../../../assets/images/teeth/active/upper_l_4_active.png";
import upper_l_5_active from "../../../../assets/images/teeth/active/upper_l_5_active.png";
import upper_l_6_active from "../../../../assets/images/teeth/active/upper_l_6_active.png";
import upper_l_7_active from "../../../../assets/images/teeth/active/upper_l_7_active.png";
import upper_l_8_active from "../../../../assets/images/teeth/active/upper_l_8_active.png";
import upper_r_1_active from "../../../../assets/images/teeth/active/upper_r_1_active.png";
import upper_r_2_active from "../../../../assets/images/teeth/active/upper_r_2_active.png";
import upper_r_3_active from "../../../../assets/images/teeth/active/upper_r_3_active.png";
import upper_r_4_active from "../../../../assets/images/teeth/active/upper_r_4_active.png";
import upper_r_5_active from "../../../../assets/images/teeth/active/upper_r_5_active.png";
import upper_r_6_active from "../../../../assets/images/teeth/active/upper_r_6_active.png";
import upper_r_7_active from "../../../../assets/images/teeth/active/upper_r_7_active.png";
import upper_r_8_active from "../../../../assets/images/teeth/active/upper_r_8_active.png";

import lower_l_1 from "../../../../assets/images/teeth/not_active/lower_l_1_not_active.png";
import lower_l_2 from "../../../../assets/images/teeth/not_active/lower_l_2_not_active.png";
import lower_l_3 from "../../../../assets/images/teeth/not_active/lower_l_3_not_active.png";
import lower_l_4 from "../../../../assets/images/teeth/not_active/lower_l_4_not_active.png";
import lower_l_5 from "../../../../assets/images/teeth/not_active/lower_l_5_not_active.png";
import lower_l_6 from "../../../../assets/images/teeth/not_active/lower_l_6_not_active.png";
import lower_l_7 from "../../../../assets/images/teeth/not_active/lower_l_7_not_active.png";
import lower_l_8 from "../../../../assets/images/teeth/not_active/lower_l_8_not_active.png";
import lower_r_1 from "../../../../assets/images/teeth/not_active/lower_r_1_not_active.png";
import lower_r_2 from "../../../../assets/images/teeth/not_active/lower_r_2_not_active.png";
import lower_r_3 from "../../../../assets/images/teeth/not_active/lower_r_3_not_active.png";
import lower_r_4 from "../../../../assets/images/teeth/not_active/lower_r_4_not_active.png";
import lower_r_5 from "../../../../assets/images/teeth/not_active/lower_r_5_not_active.png";
import lower_r_6 from "../../../../assets/images/teeth/not_active/lower_r_6_not_active.png";
import lower_r_7 from "../../../../assets/images/teeth/not_active/lower_r_7_not_active.png";
import lower_r_8 from "../../../../assets/images/teeth/not_active/lower_r_8_not_active.png";

import lower_l_1_hover from "../../../../assets/images/teeth/hover/lower_l_1_hover.png";
import lower_l_2_hover from "../../../../assets/images/teeth/hover/lower_l_2_hover.png";
import lower_l_3_hover from "../../../../assets/images/teeth/hover/lower_l_3_hover.png";
import lower_l_4_hover from "../../../../assets/images/teeth/hover/lower_l_4_hover.png";
import lower_l_5_hover from "../../../../assets/images/teeth/hover/lower_l_5_hover.png";
import lower_l_6_hover from "../../../../assets/images/teeth/hover/lower_l_6_hover.png";
import lower_l_7_hover from "../../../../assets/images/teeth/hover/lower_l_7_hover.png";
import lower_l_8_hover from "../../../../assets/images/teeth/hover/lower_l_8_hover.png";
import lower_r_1_hover from "../../../../assets/images/teeth/hover/lower_r_1_hover.png";
import lower_r_2_hover from "../../../../assets/images/teeth/hover/lower_r_2_hover.png";
import lower_r_3_hover from "../../../../assets/images/teeth/hover/lower_r_3_hover.png";
import lower_r_4_hover from "../../../../assets/images/teeth/hover/lower_r_4_hover.png";
import lower_r_5_hover from "../../../../assets/images/teeth/hover/lower_r_5_hover.png";
import lower_r_6_hover from "../../../../assets/images/teeth/hover/lower_r_6_hover.png";
import lower_r_7_hover from "../../../../assets/images/teeth/hover/lower_r_7_hover.png";
import lower_r_8_hover from "../../../../assets/images/teeth/hover/lower_r_8_hover.png";

import lower_l_1_active from "../../../../assets/images/teeth/active/lower_l_1_active.png";
import lower_l_2_active from "../../../../assets/images/teeth/active/lower_l_2_active.png";
import lower_l_3_active from "../../../../assets/images/teeth/active/lower_l_3_active.png";
import lower_l_4_active from "../../../../assets/images/teeth/active/lower_l_4_active.png";
import lower_l_5_active from "../../../../assets/images/teeth/active/lower_l_5_active.png";
import lower_l_6_active from "../../../../assets/images/teeth/active/lower_l_6_active.png";
import lower_l_7_active from "../../../../assets/images/teeth/active/lower_l_7_active.png";
import lower_l_8_active from "../../../../assets/images/teeth/active/lower_l_8_active.png";
import lower_r_1_active from "../../../../assets/images/teeth/active/lower_r_1_active.png";
import lower_r_2_active from "../../../../assets/images/teeth/active/lower_r_2_active.png";
import lower_r_3_active from "../../../../assets/images/teeth/active/lower_r_3_active.png";
import lower_r_4_active from "../../../../assets/images/teeth/active/lower_r_4_active.png";
import lower_r_5_active from "../../../../assets/images/teeth/active/lower_r_5_active.png";
import lower_r_6_active from "../../../../assets/images/teeth/active/lower_r_6_active.png";
import lower_r_7_active from "../../../../assets/images/teeth/active/lower_r_7_active.png";
import lower_r_8_active from "../../../../assets/images/teeth/active/lower_r_8_active.png";
export default class Teeth extends Component {
  constructor(props) {
    super(props);
    this.state = {
      upper_l_1: { image: upper_l_1, value: false },
      upper_l_2: { image: upper_l_2, value: false },
      upper_l_3: { image: upper_l_3, value: false },
      upper_l_4: { image: upper_l_4, value: false },
      upper_l_5: { image: upper_l_5, value: false },
      upper_l_6: { image: upper_l_6, value: false },
      upper_l_7: { image: upper_l_7, value: false },
      upper_l_8: { image: upper_l_8, value: false },

      lower_l_1: { image: lower_l_1, value: false },
      lower_l_2: { image: lower_l_2, value: false },
      lower_l_3: { image: lower_l_3, value: false },
      lower_l_4: { image: lower_l_4, value: false },
      lower_l_5: { image: lower_l_5, value: false },
      lower_l_6: { image: lower_l_6, value: false },
      lower_l_7: { image: lower_l_7, value: false },
      lower_l_8: { image: lower_l_8, value: false },

      upper_r_1: { image: upper_r_1, value: false },
      upper_r_2: { image: upper_r_2, value: false },
      upper_r_3: { image: upper_r_3, value: false },
      upper_r_4: { image: upper_r_4, value: false },
      upper_r_5: { image: upper_r_5, value: false },
      upper_r_6: { image: upper_r_6, value: false },
      upper_r_7: { image: upper_r_7, value: false },
      upper_r_8: { image: upper_r_8, value: false },

      lower_r_1: { image: lower_r_1, value: false },
      lower_r_2: { image: lower_r_2, value: false },
      lower_r_3: { image: lower_r_3, value: false },
      lower_r_4: { image: lower_r_4, value: false },
      lower_r_5: { image: lower_r_5, value: false },
      lower_r_6: { image: lower_r_6, value: false },
      lower_r_7: { image: lower_r_7, value: false },
      lower_r_8: { image: lower_r_8, value: false },
      teeth: {
        upper_l_1: {
          active: upper_l_1_active,
          not_active: upper_l_1,
          hover: upper_l_1_hover,
          image: upper_l_1,
          value: false,
        },
        upper_l_2: {
          active: upper_l_2_active,
          not_active: upper_l_2,
          hover: upper_l_2_hover,
          image: upper_l_2,
          value: false,
        },
        upper_l_3: {
          active: upper_l_3_active,
          not_active: upper_l_3,
          hover: upper_l_3_hover,
          image: upper_l_3,
          value: false,
        },
        upper_l_4: {
          active: upper_l_4_active,
          not_active: upper_l_4,
          hover: upper_l_4_hover,
          image: upper_l_4,
          value: false,
        },
        upper_l_5: {
          active: upper_l_5_active,
          not_active: upper_l_5,
          hover: upper_l_5_hover,
          image: upper_l_5,
          value: false,
        },
        upper_l_6: {
          active: upper_l_6_active,
          not_active: upper_l_6,
          hover: upper_l_6_hover,
          image: upper_l_6,
          value: false,
        },
        upper_l_7: {
          active: upper_l_7_active,
          not_active: upper_l_7,
          hover: upper_l_7_hover,
          image: upper_l_7,
          value: false,
        },
        upper_l_8: {
          active: upper_l_8_active,
          not_active: upper_l_8,
          hover: upper_l_8_hover,
          image: upper_l_8,
          value: false,
        },

        lower_l_1: {
          active: lower_l_1_active,
          not_active: lower_l_1,
          hover: lower_l_1_hover,
          image: lower_l_1,
          value: false,
        },
        lower_l_2: {
          active: lower_l_2_active,
          not_active: lower_l_2,
          hover: lower_l_2_hover,
          image: lower_l_2,
          value: false,
        },
        lower_l_3: {
          active: lower_l_3_active,
          not_active: lower_l_3,
          hover: lower_l_3_hover,
          image: lower_l_3,
          value: false,
        },
        lower_l_4: {
          active: lower_l_4_active,
          not_active: lower_l_4,
          hover: lower_l_4_hover,
          image: lower_l_4,
          value: false,
        },
        lower_l_5: {
          active: lower_l_5_active,
          not_active: lower_l_5,
          hover: lower_l_5_hover,
          image: lower_l_5,
          value: false,
        },
        lower_l_6: {
          active: lower_l_6_active,
          not_active: lower_l_6,
          hover: lower_l_6_hover,
          image: lower_l_6,
          value: false,
        },
        lower_l_7: {
          active: lower_l_7_active,
          not_active: lower_l_7,
          hover: lower_l_7_hover,
          image: lower_l_7,
          value: false,
        },
        lower_l_8: {
          active: lower_l_8_active,
          not_active: lower_l_8,
          hover: lower_l_8_hover,
          image: lower_l_8,
          value: false,
        },

        upper_r_1: {
          active: upper_r_1_active,
          not_active: upper_r_1,
          hover: upper_r_1_hover,
          image: upper_r_1,
          value: false,
        },
        upper_r_2: {
          active: upper_r_2_active,
          not_active: upper_r_2,
          hover: upper_r_2_hover,
          image: upper_r_2,
          value: false,
        },
        upper_r_3: {
          active: upper_r_3_active,
          not_active: upper_r_3,
          hover: upper_r_3_hover,
          image: upper_r_3,
          value: false,
        },
        upper_r_4: {
          active: upper_r_4_active,
          not_active: upper_r_4,
          hover: upper_r_4_hover,
          image: upper_r_4,
          value: false,
        },
        upper_r_5: {
          active: upper_r_5_active,
          not_active: upper_r_5,
          hover: upper_r_5_hover,
          image: upper_r_5,
          value: false,
        },
        upper_r_6: {
          active: upper_r_6_active,
          not_active: upper_r_6,
          hover: upper_r_6_hover,
          image: upper_r_6,
          value: false,
        },
        upper_r_7: {
          active: upper_r_7_active,
          not_active: upper_r_7,
          hover: upper_r_7_hover,
          image: upper_r_7,
          value: false,
        },
        upper_r_8: {
          active: upper_r_8_active,
          not_active: upper_r_8,
          hover: upper_r_8_hover,
          image: upper_r_8,
          value: false,
        },

        lower_r_1: {
          active: lower_r_1_active,
          not_active: lower_r_1,
          hover: lower_r_1_hover,
          image: lower_r_1,
          value: false,
        },
        lower_r_2: {
          active: lower_r_2_active,
          not_active: lower_r_2,
          hover: lower_r_2_hover,
          image: lower_r_2,
          value: false,
        },
        lower_r_3: {
          active: lower_r_3_active,
          not_active: lower_r_3,
          hover: lower_r_3_hover,
          image: lower_r_3,
          value: false,
        },
        lower_r_4: {
          active: lower_r_4_active,
          not_active: lower_r_4,
          hover: lower_r_4_hover,
          image: lower_r_4,
          value: false,
        },
        lower_r_5: {
          active: lower_r_5_active,
          not_active: lower_r_5,
          hover: lower_r_5_hover,
          image: lower_r_5,
          value: false,
        },
        lower_r_6: {
          active: lower_r_6_active,
          not_active: lower_r_6,
          hover: lower_r_6_hover,
          image: lower_r_6,
          value: false,
        },
        lower_r_7: {
          active: lower_r_7_active,
          not_active: lower_r_7,
          hover: lower_r_7_hover,
          image: lower_r_7,
          value: false,
        },
        lower_r_8: {
          active: lower_r_8_active,
          not_active: lower_r_8,
          hover: lower_r_8_hover,
          image: lower_r_8,
          value: false,
        },
      },
  
    };
  }
  toggle = (state, photo, value) => {
    const { teeth } = this.state;
    if (state === "hover" && value) {
      this.setState((prevState) => ({
        [photo]: {
          ...prevState[photo],
          image: teeth[photo].active,
        },
      }));
    } else if (state === "hover" && !value) {
      this.setState((prevState) => ({
        [photo]: {
          ...prevState[photo],
          image: teeth[photo].hover,
        },
      }));
    } else if (state === "leave" && value) {
      this.setState((prevState) => ({
        [photo]: {
          ...prevState[photo],
          image: teeth[photo].active,
        },
      }));
    } else if (state === "leave" && !value) {
      this.setState((prevState) => ({
        [photo]: {
          ...prevState[photo],
          image: teeth[photo].not_active,
        },
      }));
    } else if (state === "click") {
      const newState = !value;
      this.setState((prevState) => ({
        [photo]: {
          ...prevState[photo],
          image: newState ? teeth[photo].active : teeth[photo].not_active,
          value: !value,
        },
      }));
    } else {
      this.setState((prevState) => ({
        [photo]: {
          ...prevState[photo],
          image: teeth[photo].not_active,
          value: false,
        },
      }));
    }
  };


  render() {
    return (
      <div>
        <Row className="align-items-center justify-content-center">
          <Col xs="1" md="1" lg="1" className="pr-0 text-center">
            R
          </Col>
          <Col xs="10" md="10" lg="10" className="pr-0  pl-0 text-center">
            <div className="grid">
              <div>
                <img
                  src={this.state.upper_r_8.image}
                  onMouseEnter={() =>
                    this.toggle(
                      "hover",
                      "upper_r_8",
                      this.state.upper_r_8.value
                    )
                  }
                  onMouseLeave={() =>
                    this.toggle(
                      "leave",
                      "upper_r_8",
                      this.state.upper_r_8.value
                    )
                  }
                  onClick={() => {
                    this.toggle(
                      "click",
                      "upper_r_8",
                      this.state.upper_r_8.value
                    );
                  }}
                ></img>
              </div>
              <div>
                <img
                  src={this.state.upper_r_7.image}
                  onMouseEnter={() =>
                    this.toggle(
                      "hover",
                      "upper_r_7",
                      this.state.upper_r_7.value
                    )
                  }
                  onMouseLeave={() =>
                    this.toggle(
                      "leave",
                      "upper_r_7",
                      this.state.upper_r_7.value
                    )
                  }
                  onClick={() => {
                    this.toggle(
                      "click",
                      "upper_r_7",
                      this.state.upper_r_7.value
                    );
                  }}
                ></img>
              </div>
              <div>
                <img
                  src={this.state.upper_r_6.image}
                  onMouseEnter={() =>
                    this.toggle(
                      "hover",
                      "upper_r_6",
                      this.state.upper_r_6.value
                    )
                  }
                  onMouseLeave={() =>
                    this.toggle(
                      "leave",
                      "upper_r_6",
                      this.state.upper_r_6.value
                    )
                  }
                  onClick={() => {
                    this.toggle(
                      "click",
                      "upper_r_6",
                      this.state.upper_r_6.value
                    );
                  }}
                ></img>
              </div>
              <div>
                <img
                  src={this.state.upper_r_5.image}
                  onMouseEnter={() =>
                    this.toggle(
                      "hover",
                      "upper_r_5",
                      this.state.upper_r_5.value
                    )
                  }
                  onMouseLeave={() =>
                    this.toggle(
                      "leave",
                      "upper_r_5",
                      this.state.upper_r_5.value
                    )
                  }
                  onClick={() => {
                    this.toggle(
                      "click",
                      "upper_r_5",
                      this.state.upper_r_5.value
                    );
                  }}
                ></img>
              </div>
              <div>
                <img
                  src={this.state.upper_r_4.image}
                  onMouseEnter={() =>
                    this.toggle(
                      "hover",
                      "upper_r_4",
                      this.state.upper_r_4.value
                    )
                  }
                  onMouseLeave={() =>
                    this.toggle(
                      "leave",
                      "upper_r_4",
                      this.state.upper_r_4.value
                    )
                  }
                  onClick={() => {
                    this.toggle(
                      "click",
                      "upper_r_4",
                      this.state.upper_r_4.value
                    );
                  }}
                ></img>
              </div>
              <div>
                <img
                  src={this.state.upper_r_3.image}
                  onMouseEnter={() =>
                    this.toggle(
                      "hover",
                      "upper_r_3",
                      this.state.upper_r_3.value
                    )
                  }
                  onMouseLeave={() =>
                    this.toggle(
                      "leave",
                      "upper_r_3",
                      this.state.upper_r_3.value
                    )
                  }
                  onClick={() => {
                    this.toggle(
                      "click",
                      "upper_r_3",
                      this.state.upper_r_3.value
                    );
                  }}
                ></img>
              </div>
              <div>
                <img
                  src={this.state.upper_r_2.image}
                  onMouseEnter={() =>
                    this.toggle(
                      "hover",
                      "upper_r_2",
                      this.state.upper_r_2.value
                    )
                  }
                  onMouseLeave={() =>
                    this.toggle(
                      "leave",
                      "upper_r_2",
                      this.state.upper_r_2.value
                    )
                  }
                  onClick={() => {
                    this.toggle(
                      "click",
                      "upper_r_2",
                      this.state.upper_r_2.value
                    );
                  }}
                ></img>
              </div>
              <div>
                <img
                  src={this.state.upper_r_1.image}
                  onMouseEnter={() =>
                    this.toggle(
                      "hover",
                      "upper_r_1",
                      this.state.upper_r_1.value
                    )
                  }
                  onMouseLeave={() =>
                    this.toggle(
                      "leave",
                      "upper_r_1",
                      this.state.upper_r_1.value
                    )
                  }
                  onClick={() => {
                    this.toggle(
                      "click",
                      "upper_r_1",
                      this.state.upper_r_1.value
                    );
                  }}
                ></img>
              </div>
              <div>
                <img
                  src={this.state.upper_l_1.image}
                  onMouseEnter={() =>
                    this.toggle(
                      "hover",
                      "upper_l_1",
                      this.state.upper_l_1.value
                    )
                  }
                  onMouseLeave={() =>
                    this.toggle(
                      "leave",
                      "upper_l_1",
                      this.state.upper_l_1.value
                    )
                  }
                  onClick={() => {
                    this.toggle(
                      "click",
                      "upper_l_1",
                      this.state.upper_l_1.value
                    );
                  }}
                ></img>
              </div>
              <div>
                <img
                  src={this.state.upper_l_2.image}
                  onMouseEnter={() =>
                    this.toggle(
                      "hover",
                      "upper_l_2",
                      this.state.upper_l_2.value
                    )
                  }
                  onMouseLeave={() =>
                    this.toggle(
                      "leave",
                      "upper_l_2",
                      this.state.upper_l_2.value
                    )
                  }
                  onClick={() => {
                    this.toggle(
                      "click",
                      "upper_l_2",
                      this.state.upper_l_2.value
                    );
                  }}
                ></img>
              </div>
              <div>
                <img
                  src={this.state.upper_l_3.image}
                  onMouseEnter={() =>
                    this.toggle(
                      "hover",
                      "upper_l_3",
                      this.state.upper_l_3.value
                    )
                  }
                  onMouseLeave={() =>
                    this.toggle(
                      "leave",
                      "upper_l_3",
                      this.state.upper_l_3.value
                    )
                  }
                  onClick={() => {
                    this.toggle(
                      "click",
                      "upper_l_3",
                      this.state.upper_l_3.value
                    );
                  }}
                ></img>
              </div>
              <div>
                <img
                  src={this.state.upper_l_4.image}
                  onMouseEnter={() =>
                    this.toggle(
                      "hover",
                      "upper_l_4",
                      this.state.upper_l_4.value
                    )
                  }
                  onMouseLeave={() =>
                    this.toggle(
                      "leave",
                      "upper_l_4",
                      this.state.upper_l_4.value
                    )
                  }
                  onClick={() => {
                    this.toggle(
                      "click",
                      "upper_l_4",
                      this.state.upper_l_4.value
                    );
                  }}
                ></img>
              </div>
              <div>
                <img
                  src={this.state.upper_l_5.image}
                  onMouseEnter={() =>
                    this.toggle(
                      "hover",
                      "upper_l_5",
                      this.state.upper_l_5.value
                    )
                  }
                  onMouseLeave={() =>
                    this.toggle(
                      "leave",
                      "upper_l_5",
                      this.state.upper_l_5.value
                    )
                  }
                  onClick={() => {
                    this.toggle(
                      "click",
                      "upper_l_5",
                      this.state.upper_l_5.value
                    );
                  }}
                ></img>
              </div>
              <div>
                <img
                  src={this.state.upper_l_6.image}
                  onMouseEnter={() =>
                    this.toggle(
                      "hover",
                      "upper_l_6",
                      this.state.upper_l_6.value
                    )
                  }
                  onMouseLeave={() =>
                    this.toggle(
                      "leave",
                      "upper_l_6",
                      this.state.upper_l_6.value
                    )
                  }
                  onClick={() => {
                    this.toggle(
                      "click",
                      "upper_l_6",
                      this.state.upper_l_6.value
                    );
                  }}
                ></img>
              </div>
              <div>
                <img
                  src={this.state.upper_l_7.image}
                  onMouseEnter={() =>
                    this.toggle(
                      "hover",
                      "upper_l_7",
                      this.state.upper_l_7.value
                    )
                  }
                  onMouseLeave={() =>
                    this.toggle(
                      "leave",
                      "upper_l_7",
                      this.state.upper_l_7.value
                    )
                  }
                  onClick={() => {
                    this.toggle(
                      "click",
                      "upper_l_7",
                      this.state.upper_l_7.value
                    );
                  }}
                ></img>
              </div>
              <div>
                <img
                  src={this.state.upper_l_8.image}
                  onMouseEnter={() =>
                    this.toggle(
                      "hover",
                      "upper_l_8",
                      this.state.upper_l_8.value
                    )
                  }
                  onMouseLeave={() =>
                    this.toggle(
                      "leave",
                      "upper_l_8",
                      this.state.upper_l_8.value
                    )
                  }
                  onClick={() => {
                    this.toggle(
                      "click",
                      "upper_l_8",
                      this.state.upper_l_8.value
                    );
                  }}
                ></img>
              </div>
            </div>
            <div className="grid text-center">
              <div className="text-center">8</div>
              <div className="text-center">7</div>
              <div className="text-center">6</div>
              <div className="text-center">5</div>
              <div className="text-center">4</div>
              <div className="text-center">3</div>
              <div className="text-center">2</div>
              <div className="text-center">1</div>
              <div className="text-center">1</div>
              <div className="text-center">2</div>
              <div className="text-center">3</div>
              <div className="text-center">4</div>
              <div className="text-center">5</div>
              <div className="text-center">6</div>
              <div className="text-center">7</div>
              <div className="text-center">8</div>
            </div>
            
            <div className="grid">
              <div>
                <img
                  src={this.state.lower_r_8.image}
                  onMouseEnter={() =>
                    this.toggle(
                      "hover",
                      "lower_r_8",
                      this.state.lower_r_8.value
                    )
                  }
                  onMouseLeave={() =>
                    this.toggle(
                      "leave",
                      "lower_r_8",
                      this.state.lower_r_8.value
                    )
                  }
                  onClick={() => {
                    this.toggle(
                      "click",
                      "lower_r_8",
                      this.state.lower_r_8.value
                    );
                  }}
                ></img>
              </div>
              <div>
                <img
                  src={this.state.lower_r_7.image}
                  onMouseEnter={() =>
                    this.toggle(
                      "hover",
                      "lower_r_7",
                      this.state.lower_r_7.value
                    )
                  }
                  onMouseLeave={() =>
                    this.toggle(
                      "leave",
                      "lower_r_7",
                      this.state.lower_r_7.value
                    )
                  }
                  onClick={() => {
                    this.toggle(
                      "click",
                      "lower_r_7",
                      this.state.lower_r_7.value
                    );
                  }}
                ></img>
              </div>
              <div>
                <img
                  src={this.state.lower_r_6.image}
                  onMouseEnter={() =>
                    this.toggle(
                      "hover",
                      "lower_r_6",
                      this.state.lower_r_6.value
                    )
                  }
                  onMouseLeave={() =>
                    this.toggle(
                      "leave",
                      "lower_r_6",
                      this.state.lower_r_6.value
                    )
                  }
                  onClick={() => {
                    this.toggle(
                      "click",
                      "lower_r_6",
                      this.state.lower_r_6.value
                    );
                  }}
                ></img>
              </div>
              <div>
                <img
                  src={this.state.lower_r_5.image}
                  onMouseEnter={() =>
                    this.toggle(
                      "hover",
                      "lower_r_5",
                      this.state.lower_r_5.value
                    )
                  }
                  onMouseLeave={() =>
                    this.toggle(
                      "leave",
                      "lower_r_5",
                      this.state.lower_r_5.value
                    )
                  }
                  onClick={() => {
                    this.toggle(
                      "click",
                      "lower_r_5",
                      this.state.lower_r_5.value
                    );
                  }}
                ></img>
              </div>
              <div>
                <img
                  src={this.state.lower_r_4.image}
                  onMouseEnter={() =>
                    this.toggle(
                      "hover",
                      "lower_r_4",
                      this.state.lower_r_4.value
                    )
                  }
                  onMouseLeave={() =>
                    this.toggle(
                      "leave",
                      "lower_r_4",
                      this.state.lower_r_4.value
                    )
                  }
                  onClick={() => {
                    this.toggle(
                      "click",
                      "lower_r_4",
                      this.state.lower_r_4.value
                    );
                  }}
                ></img>
              </div>
              <div>
                <img
                  src={this.state.lower_r_3.image}
                  onMouseEnter={() =>
                    this.toggle(
                      "hover",
                      "lower_r_3",
                      this.state.lower_r_3.value
                    )
                  }
                  onMouseLeave={() =>
                    this.toggle(
                      "leave",
                      "lower_r_3",
                      this.state.lower_r_3.value
                    )
                  }
                  onClick={() => {
                    this.toggle(
                      "click",
                      "lower_r_3",
                      this.state.lower_r_3.value
                    );
                  }}
                ></img>
              </div>
              <div>
                <img
                  src={this.state.lower_r_2.image}
                  onMouseEnter={() =>
                    this.toggle(
                      "hover",
                      "lower_r_2",
                      this.state.lower_r_2.value
                    )
                  }
                  onMouseLeave={() =>
                    this.toggle(
                      "leave",
                      "lower_r_2",
                      this.state.lower_r_2.value
                    )
                  }
                  onClick={() => {
                    this.toggle(
                      "click",
                      "lower_r_2",
                      this.state.lower_r_2.value
                    );
                  }}
                ></img>
              </div>
              <div>
                <img
                  src={this.state.lower_r_1.image}
                  onMouseEnter={() =>
                    this.toggle(
                      "hover",
                      "lower_r_1",
                      this.state.lower_r_1.value
                    )
                  }
                  onMouseLeave={() =>
                    this.toggle(
                      "leave",
                      "lower_r_1",
                      this.state.lower_r_1.value
                    )
                  }
                  onClick={() => {
                    this.toggle(
                      "click",
                      "lower_r_1",
                      this.state.lower_r_1.value
                    );
                  }}
                ></img>
              </div>
              <div>
                <img
                  src={this.state.lower_l_1.image}
                  onMouseEnter={() =>
                    this.toggle(
                      "hover",
                      "lower_l_1",
                      this.state.lower_l_1.value
                    )
                  }
                  onMouseLeave={() =>
                    this.toggle(
                      "leave",
                      "lower_l_1",
                      this.state.lower_l_1.value
                    )
                  }
                  onClick={() => {
                    this.toggle(
                      "click",
                      "lower_l_1",
                      this.state.lower_l_1.value
                    );
                  }}
                ></img>
              </div>
              <div>
                <img
                  src={this.state.lower_l_2.image}
                  onMouseEnter={() =>
                    this.toggle(
                      "hover",
                      "lower_l_2",
                      this.state.lower_l_2.value
                    )
                  }
                  onMouseLeave={() =>
                    this.toggle(
                      "leave",
                      "lower_l_2",
                      this.state.lower_l_2.value
                    )
                  }
                  onClick={() => {
                    this.toggle(
                      "click",
                      "lower_l_2",
                      this.state.lower_l_2.value
                    );
                  }}
                ></img>
              </div>
              <div>
                <img
                  src={this.state.lower_l_3.image}
                  onMouseEnter={() =>
                    this.toggle(
                      "hover",
                      "lower_l_3",
                      this.state.lower_l_3.value
                    )
                  }
                  onMouseLeave={() =>
                    this.toggle(
                      "leave",
                      "lower_l_3",
                      this.state.lower_l_3.value
                    )
                  }
                  onClick={() => {
                    this.toggle(
                      "click",
                      "lower_l_3",
                      this.state.lower_l_3.value
                    );
                  }}
                ></img>
              </div>
              <div>
                <img
                  src={this.state.lower_l_4.image}
                  onMouseEnter={() =>
                    this.toggle(
                      "hover",
                      "lower_l_4",
                      this.state.lower_l_4.value
                    )
                  }
                  onMouseLeave={() =>
                    this.toggle(
                      "leave",
                      "lower_l_4",
                      this.state.lower_l_4.value
                    )
                  }
                  onClick={() => {
                    this.toggle(
                      "click",
                      "lower_l_4",
                      this.state.lower_l_4.value
                    );
                  }}
                ></img>
              </div>
              <div>
                <img
                  src={this.state.lower_l_5.image}
                  onMouseEnter={() =>
                    this.toggle(
                      "hover",
                      "lower_l_5",
                      this.state.lower_l_5.value
                    )
                  }
                  onMouseLeave={() =>
                    this.toggle(
                      "leave",
                      "lower_l_5",
                      this.state.lower_l_5.value
                    )
                  }
                  onClick={() => {
                    this.toggle(
                      "click",
                      "lower_l_5",
                      this.state.lower_l_5.value
                    );
                  }}
                ></img>
              </div>
              <div>
                <img
                  src={this.state.lower_l_6.image}
                  onMouseEnter={() =>
                    this.toggle(
                      "hover",
                      "lower_l_6",
                      this.state.lower_l_6.value
                    )
                  }
                  onMouseLeave={() =>
                    this.toggle(
                      "leave",
                      "lower_l_6",
                      this.state.lower_l_6.value
                    )
                  }
                  onClick={() => {
                    this.toggle(
                      "click",
                      "lower_l_6",
                      this.state.lower_l_6.value
                    );
                  }}
                ></img>
              </div>
              <div>
                <img
                  src={this.state.lower_l_7.image}
                  onMouseEnter={() =>
                    this.toggle(
                      "hover",
                      "lower_l_7",
                      this.state.lower_l_7.value
                    )
                  }
                  onMouseLeave={() =>
                    this.toggle(
                      "leave",
                      "lower_l_7",
                      this.state.lower_l_7.value
                    )
                  }
                  onClick={() => {
                    this.toggle(
                      "click",
                      "lower_l_7",
                      this.state.lower_l_7.value
                    );
                  }}
                ></img>
              </div>
              <div>
                <img
                  src={this.state.lower_l_8.image}
                  onMouseEnter={() =>
                    this.toggle(
                      "hover",
                      "lower_l_8",
                      this.state.lower_l_8.value
                    )
                  }
                  onMouseLeave={() =>
                    this.toggle(
                      "leave",
                      "lower_l_8",
                      this.state.lower_l_8.value
                    )
                  }
                  onClick={() => {
                    this.toggle(
                      "click",
                      "lower_l_8",
                      this.state.lower_l_8.value
                    );
                  }}
                ></img>
              </div>
            </div>
         
          </Col>
          <Col xs="1" md="1" lg="1" className="pl-0 text-center">
            L
          </Col>
        </Row>
   
      </div>
    );
  }
}
