import React, { Component, Fragment } from 'react';
import { Button, Card, CardBody, Row, Col, Table } from 'reactstrap';

export default class InvoiceList extends Component {

    // Data
    state = {
        items: [
            {
                itemid: 1,
                itemname: 'Triple camera smartphones '
            },
            {
                itemid: 2,
                itemname: 'Headsets for all styles '
            }
        ]
    }
    // Condition for view detail
    state = { showing: true };

    render() {
        const { showing } = this.state;
        return (
            <tr>
                <td>{this.props.id}</td>
                <td>
                    <h6 className="text-dark font-medium mb-0">{this.props.billfrom}</h6>
                    <span className="text-muted">{this.props.billfromemail}</span>
                </td>
                <td>{this.props.billto}</td>
                <td>{this.props.total}</td>
                <td><span className="badge badge-primary">{this.props.status}</span></td>
                <td>
                    <Button className="btn" color="danger" onClick={this.props.delete}><i className="ti-trash"></i></Button>
                    <Button className="btn ml-2" color="info" onClick={() => this.setState({ showing: !showing })}><i className="ti-eye"></i></Button>
                </td>
                <Fragment>
                		<Card className="card-invoice-detail border-0" style={{ display: (showing ? 'none' : 'block') }}>
                    <CardBody>
                        <div className="d-flex align-items-center border-dash-bottom pb-3">
                            <h3 className="mb-0 text-dark font-weight-bold">INVOICE #{this.props.id}</h3>
                            <div className="ml-auto">
                                <Button className="btn ml-2" outline color="primary" onClick={() => this.setState({ showing: !showing })}><i className="fas fa-chevron-left mr-1"></i>Go to Invoice</Button>
                            </div>
                        </div>
                        <div className="py-3 d-flex align-items-start bg-light border-dash-bottom px-2">
                            <div>
                                <h3 className="mb-0 text-danger font-medium">{this.props.billtitle}</h3>
                                <h5 className="mb-0 text-muted font-italic">{this.props.billfrom}</h5>
                                <div className="text-muted w-50 text-justify mt-2">
                                    {this.props.fromaddress}
                                </div>
                            </div>
                            <div className="ml-auto text-right">
                                <h5 className="mb-0">Order Status:</h5>
                                <span className="badge badge-primary">{this.props.status}</span>
                                <h5 className="mb-0 mt-2">Invoice Date:</h5>
                                <span className="text-muted">{this.props.idate}</span>
                                <h5 className="mb-0 mt-2">Due Date:</h5>
                                <span className="text-muted">{this.props.duedate}</span>
                            </div>
                        </div>
                        <Row>
                            <Col sm={{ size: 'auto', offset: 5 }}>
                                <div className="text-right py-3">
                                    <h3>To,</h3>
                                    <h4 className="font-italic">{this.props.billto}</h4>
                                    <div className="text-muted text-justify w-50 text-right mt-2">
                                        {this.props.toaddress}
                                    </div>
                                    <span className="text-dark font-medium">Fax: </span><span>{this.props.tofax}</span><br />
                                    <span className="text-dark font-medium">Phone: </span><span>{this.props.tophone}</span>
                                </div>
                            </Col>
                        </Row>
                        
                        <Table>
                            <thead className="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>Item Name</th>
                                    <th>Unit Costs</th>
                                    <th>Unit</th>
                                    <th>Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                {/* {
                                    this.state.items.map((subItem, index) => {
                                        return (
                                            <InvoiceTable
                                                key={subItem.itemid}
                                                itemid={subItem.itemid}
                                                itemname={subItem.itemname}
                                            />
                                        )
                                    })
                                } */}
                            </tbody>
                        </Table>
                    </CardBody>
                		</Card>
                </Fragment>                
            </tr>
        )
    }
}