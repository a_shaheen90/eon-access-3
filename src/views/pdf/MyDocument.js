import React, { Component } from "react";
import { Page, Text, Document, StyleSheet } from "@react-pdf/renderer";
import PDFHeader from "./PDFComponents/PDFHeader";
import DoctorInfo from "./PDFComponents/DoctorInfo";
import PatientInfo from "./PDFComponents/PatientInfo";
import ClinicalFindingsPDF from "./PDFComponents/ClinicalFindingsPDF";
import TreatmentGoalPDF from "./PDFComponents/TreatmentGoalPDF";
import SummaryPdf from "./PDFComponents/SummaryPDF";
import Footer from "./PDFComponents/Footer";
import ShppingAddress from "./PDFComponents/ShppingAddress";
// Create styles
const borderColor = "#90e5fc";

const styles = StyleSheet.create({
  body: {
    paddingTop: 35,
    paddingBottom: 65,
    paddingHorizontal: 35,
  },
  title: {
    fontSize: 24,
    textAlign: "center",
  },
  author: {
    fontSize: 12,
    textAlign: "center",
    marginBottom: 20,
    marginTop: 5,
  },
  subtitle: {
    fontSize: 18,
    margin: 12,
  },
  text: {
    margin: 12,
    fontSize: 14,
    textAlign: "justify",
    fontFamily: "Times-Roman",
  },
  image: {
    marginVertical: 15,
    marginHorizontal: 100,
    marginBottom: 20,
    width: "10%",
    height: "10%",
    backgroundColor: "red",
    borderRadius: 50,
  },
  header: {
    fontSize: 14,
    textAlign: "left",
    fontWeight: "bold",
  },
  pageNumber: {
    position: "absolute",
    fontSize: 12,
    bottom: 30,
    left: 0,
    right: 0,
    textAlign: "center",
    color: "grey",
  },
  section: {
    margin: 10,
    padding: 10,
    flexGrow: 1,
  },
  row: {
    flexDirection: "row",
  },
  col: {
    display: "flex",
    flexDirection: "column",
    fontSize: 14,
    padding: 5,
  },
  value: {
    display: "flex",
    flexDirection: "column",
    fontSize: 12,
    fontWeight: "normal",
    padding: 5,
  },

  box: {
    flexDirection: "row",
    flexWrap: "wrap",
    marginTop: 24,
    borderWidth: 1,
    borderColor: "#bff0fd",
  },

  container: {
    flexDirection: "row",
    borderBottomColor: "#bff0fd",
    backgroundColor: "#bff0fd",
    borderBottomWidth: 1,
    alignItems: "center",
    height: 24,
    textAlign: "center",
    fontStyle: "bold",
    flexGrow: 1,
  },
  description: {
    width: "25%",
    borderRightColor: borderColor,
    borderRightWidth: 1,
  },
  qty: {
    width: "25%",
    borderRightColor: borderColor,
    borderRightWidth: 1,
  },
  rate: {
    width: "25%",
    borderRightColor: borderColor,
    borderRightWidth: 1,
  },
  amount: {
    width: "25%",
  },

  row1: {
    flexDirection: "row",
    borderBottomColor: "#bff0fd",
    borderBottomWidth: 1,
    alignItems: "center",
    height: 24,
    fontStyle: "bold",
  },
  description1: {
    width: "25%",
    textAlign: "center",
    borderRightColor: borderColor,
    borderRightWidth: 1,
  },
  qty1: {
    width: "25%",
    borderRightColor: borderColor,
    borderRightWidth: 1,
    textAlign: "center",
  },
  rate1: {
    width: "25%",
    borderRightColor: borderColor,
    borderRightWidth: 1,
    textAlign: "center",
  },
  amount1: {
    width: "25%",
    textAlign: "center",
  },
  tableContainer: {
    flexDirection: "row",
    flexWrap: "wrap",
    marginTop: 24,
    borderWidth: 1,
    borderColor: "#bff0fd",
    fontSize: 12,
    textAlign: "center",
  },
});

// Create Document Component
export default class MyDocument extends Component {
  constructor(props) {
    super(props);
    this.state = {
      caseInfo: props.case,
      doctorInfo: JSON.parse(localStorage.getItem("currentUser")),
      mappedData: props.mappedData,
    };
  }
  render() {
    const { caseInfo, doctorInfo, mappedData } = this.state;
    return (
      <Document>
        <Page style={styles.body}>
          <PDFHeader />
          <DoctorInfo doctorInfo={doctorInfo} case={caseInfo} />
          <ShppingAddress case={caseInfo}/>
          <PatientInfo case={caseInfo} />
          <ClinicalFindingsPDF mappedData={mappedData} />
          <Footer doctorInfo={doctorInfo} />
          <Text
            style={styles.pageNumber}
            render={({ pageNumber, totalPages }) =>
              `${pageNumber} / ${totalPages}`
            }
            fixed
          />
        </Page>

        <Page style={styles.body}>
          <TreatmentGoalPDF mappedData={mappedData} />
          <SummaryPdf mappedData={mappedData} />
          <Footer doctorInfo={doctorInfo} />

          <Text
            style={styles.pageNumber}
            render={({ pageNumber, totalPages }) =>
              `${pageNumber} / ${totalPages}`
            }
            fixed
          />
        </Page>
      </Document>
    );
  }
}
