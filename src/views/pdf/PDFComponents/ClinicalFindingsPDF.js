import React, { Component, Fragment } from "react";
import { Text, StyleSheet, View } from "@react-pdf/renderer";
import TeethPDF from "./TeethPDF";
const styles = StyleSheet.create({
  row: {
    paddingTop: 10,
    alignItems: "center",
    paddingBottom: 10,
  },

  header: {
    fontSize: 12,
    textAlign: "left",
    color: "#00c7b1",
    width: "100%",
  },
  container: {
    flexDirection: "row",
    // borderBottomWidth: 1,
    // borderColor: "gray",
    paddingBottom: 20,
    paddingTop: 10,
  },
  container2: {
    flexDirection: "row",
  },
  field: {
    flexDirection: "row",
    paddingTop: 10,
  },
  col: {
    flexDirection: "column",
    width: "50%",
  },
  col_4: {
    flexDirection: "column",
    width: "40%",
  },
  col_5: {
    flexDirection: "column",
    width: "50%",
  },
  col_1: {
    flexDirection: "column",
    width: "10%",
  },
  title: {
    fontSize: 10,
    textAlign: "left",
    color: "#00c7b1",
    padding: "10px 0 5px 0",
  },
  title2: {
    fontSize: 10,
    textAlign: "left",
    color: "#00c7b1",
  },
  info: {
    fontSize: 10,
    textAlign: "left",
  },
  image: {
    marginVertical: 10,
    width: "60%",
    height: "50%",
    backgroundColor: "red",
    borderRadius: 8,
  },

  green_btn_not_active: {
    flexGrow: 1,
    alignItems: "center",
    justifyContent: "center",
    borderColor: "#d7f5f2",
    borderWidth: 1,
    backgroundColor: "#d7f5f2",
    borderRadius: 8,
    fontSize: 8,
    padding: 2,
    margin: 3,
    flexDirection: "column",
    width: "30%",
  },
  col_title: {
    flexGrow: 1,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 8,
    fontSize: 8,
    padding: "0 5px 0 0",
    // margin: 1,
    flexDirection: "column",
    width: "10%",
  },
  green_btn_active: {
    flexGrow: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#00c7b1",
    borderRadius: 8,
    fontSize: 8,
    padding: 2,
    margin: 3,
    flexDirection: "column",
    width: "30%",
  },
  emplty_col: {
    width: "30%",
  },
  text_active: {
    fontSize: 8,
    color: "white",
    padding: 1,
  },
  text_not_active: {
    fontSize: 8,
    color: "#00c7b1",
    padding: 1,
  },
  teeth_border: {
    borderWidth: 1,
    borderColor: "#00c7b1",
  },
  text_area: {
    flexGrow: 1,
    borderColor: "#a0f0e7",
    borderWidth: 1,
    borderRadius: 2,
    fontSize: 8,
    padding: 2,
    margin: 3,
    minHeight: "50px",
    flexDirection: "column",
  },
  value: {
    padding: "10px",
  },
});
export default class ClinicalFindingsPDF extends Component {
  constructor(props) {
    super(props);

    this.state = {
      clinical_findings: props.mappedData.treatmentPlan.clinical_findings,
    };
  }
  render() {
    const { clinical_findings } = this.state;
    return (
      <Fragment>
        <View style={styles.row}>
          <Text style={styles.header}>Clinical Findings :</Text>
          <View style={styles.container}>
            <View style={styles.col_4}>
              <Text style={styles.title2}>Chief Complaint : </Text>
              <View style={styles.text_area}>
                <Text style={styles.value}>
                  {clinical_findings.chief_complaint}
                </Text>
              </View>
              <Text style={styles.title}>Skeletal Relationship : </Text>
              <View style={styles.container2}>
                <View style={styles.col_title}>
                  <Text style={styles.text_active}>{` `}</Text>
                </View>
                <View
                  style={
                    clinical_findings.skeletal_1
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      clinical_findings.skeletal_1
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Class I
                  </Text>
                </View>
                <View
                  style={
                    clinical_findings.skeletal_2
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      clinical_findings.skeletal_2
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Class II
                  </Text>
                </View>
                <View
                  style={
                    clinical_findings.skeletal_3
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      clinical_findings.skeletal_3
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Class III
                  </Text>
                </View>
              </View>
              <Text style={styles.title}>Canine Relationship : </Text>
              <View style={styles.container2}>
                <View style={styles.col_title}>
                  <Text style={styles.text_not_active}>R</Text>
                </View>
                <View
                  style={
                    clinical_findings.canine_r_1
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      clinical_findings.canine_r_1
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Class I
                  </Text>
                </View>
                <View
                  style={
                    clinical_findings.canine_r_2
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      clinical_findings.canine_r_2
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Class II
                  </Text>
                </View>
                <View
                  style={
                    clinical_findings.canine_r_3
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      clinical_findings.canine_r_3
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Class III
                  </Text>
                </View>
              </View>
              <View style={styles.container2}>
                <View style={styles.col_title}>
                  <Text style={styles.text_not_active}>L</Text>
                </View>
                <View
                  style={
                    clinical_findings.canine_l_1
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      clinical_findings.canine_l_1
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Class I
                  </Text>
                </View>
                <View
                  style={
                    clinical_findings.canine_l_2
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      clinical_findings.canine_l_2
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Class II
                  </Text>
                </View>
                <View
                  style={
                    clinical_findings.canine_l_3
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      clinical_findings.canine_l_3
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Class III
                  </Text>
                </View>
              </View>

              <Text style={styles.title}>Molar Relationship : </Text>
              <View style={styles.container2}>
                <View style={styles.col_title}>
                  <Text style={styles.text_not_active}>R</Text>
                </View>
                <View
                  style={
                    clinical_findings.molar_r_1
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      clinical_findings.molar_r_1
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Class I
                  </Text>
                </View>
                <View
                  style={
                    clinical_findings.molar_r_2
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      clinical_findings.molar_r_2
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Class II
                  </Text>
                </View>
                <View
                  style={
                    clinical_findings.molar_r_3
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      clinical_findings.molar_r_3
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Class III
                  </Text>
                </View>
              </View>
              <View style={styles.container2}>
                <View style={styles.col_title}>
                  <Text style={styles.text_not_active}>L</Text>
                </View>
                <View
                  style={
                    clinical_findings.molar_l_1
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      clinical_findings.molar_l_1
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Class I
                  </Text>
                </View>
                <View
                  style={
                    clinical_findings.molar_l_2
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      clinical_findings.molar_l_2
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Class II
                  </Text>
                </View>
                <View
                  style={
                    clinical_findings.molar_l_3
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      clinical_findings.molar_l_3
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Class III
                  </Text>
                </View>
              </View>
            </View>
            <View style={styles.col_1}></View>
            <View style={styles.col_5}>
              <Text style={styles.title}>Tooth Movement Restrictions : </Text>
              <TeethPDF teeth={clinical_findings.Tooth_Movement_Restrictions} />

              <Text style={styles.title}>Do not place attachments : </Text>
              <TeethPDF teeth={clinical_findings.Do_not_place_attachments} />

              <Text style={styles.title}>Upper Midline : </Text>
              <View style={styles.container2}>
                <View
                  style={
                    clinical_findings.upper_midline_center
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      clinical_findings.upper_midline_center
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Centered
                  </Text>
                </View>
                <View
                  style={
                    clinical_findings.upper_midline_shifted_left
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      clinical_findings.upper_midline_shifted_left
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Shifted Left
                  </Text>
                </View>
                <View
                  style={
                    clinical_findings.upper_midline_shifted_right
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      clinical_findings.upper_midline_shifted_right
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Shifted Right
                  </Text>
                </View>
              </View>

              <Text style={styles.title}>Lower Midline : </Text>
              <View style={styles.container2}>
                <View
                  style={
                    clinical_findings.lower_midline_center
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      clinical_findings.lower_midline_center
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Centered
                  </Text>
                </View>
                <View
                  style={
                    clinical_findings.lower_midline_shifted_left
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      clinical_findings.lower_midline_shifted_left
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Shifted Left
                  </Text>
                </View>
                <View
                  style={
                    clinical_findings.lower_midline_shifted_right
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      clinical_findings.lower_midline_shifted_right
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Shifted Right
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </View>
      </Fragment>
    );
  }
}
