import React, { Component, Fragment } from "react";
import { Text, StyleSheet, View } from "@react-pdf/renderer";
import TeethPDF from "./TeethPDF";
const styles = StyleSheet.create({
  row: {
    paddingTop: 10,
    alignItems: "center",
    paddingBottom: 10,
  },

  header: {
    fontSize: 12,
    textAlign: "left",
    color: "#00c7b1",
    width: "100%",
  },
  container: {
    flexDirection: "row",
    borderBottomWidth: 1,
    borderColor: "#E6E6FA",
    paddingBottom: 20,
    paddingTop: 5,
  },
  container2: {
    flexDirection: "row",
  },
  field: {
    flexDirection: "row",
    paddingTop: 10,
  },
  col: {
    flexDirection: "column",
    width: "50%",
  },
  col_4: {
    flexDirection: "column",
    width: "40%",
  },
  col_5: {
    flexDirection: "column",
    width: "50%",
  },
  col_1: {
    flexDirection: "column",
    width: "10%",
  },
  title: {
    fontSize: 10,
    textAlign: "left",
    color: "#00c7b1",
    padding: "10px 0 5px 0",
  },
  title2: {
    fontSize: 10,
    textAlign: "left",
    color: "#00c7b1",
  },
  info: {
    fontSize: 10,
    textAlign: "left",
  },
  image: {
    marginVertical: 10,
    width: "60%",
    height: "50%",
    backgroundColor: "red",
    borderRadius: 8,
  },

  green_btn_not_active: {
    flexGrow: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#d7f5f2",
    borderRadius: 8,
    fontSize: 8,
    padding: 2,
    margin: 3,
    flexDirection: "column",
    width: "30%",
  },
  col_title: {
    flexGrow: 1,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 8,
    fontSize: 8,
    padding: "0 5px 0 0",
    // margin: 1,
    flexDirection: "column",
    width: "10%",
  },
  green_btn_active: {
    flexGrow: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#00c7b1",
    borderRadius: 8,
    fontSize: 8,
    padding: 2,
    margin: 3,
    flexDirection: "column",
    width: "30%",
  },
  text_active: {
    fontSize: 8,
    color: "white",
    padding: 1,
  },
  text_not_active: {
    fontSize: 8,
    color: "#00c7b1",
    padding: 1,
  },
  emplty_col: {
    width: "30%",
  },
});
export default class TreatmentGoalPDF extends Component {
  constructor(props) {
    super(props);

    this.state = {
      treatment_goals: props.mappedData.treatmentPlan.treatment_goals,
    };
  }
  render() {
    const { treatment_goals } = this.state;
    return (
      <Fragment>
        <View style={styles.row}>
          <Text style={styles.header}>Treatment Goals :</Text>
          <View style={styles.container}>
            <View style={styles.col_4}>
              <Text style={styles.title}>Treat Arches : </Text>
              <View style={styles.container2}>
                <View style={styles.col_title}>
                  <Text style={styles.text_active}>{` `}</Text>
                </View>
                <View
                  style={
                    treatment_goals.treat_arches_upper
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.treat_arches_upper
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Upper Only
                  </Text>
                </View>
                <View
                  style={
                    treatment_goals.treat_arches_lower
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.treat_arches_lower
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Lower Only
                  </Text>
                </View>
                <View
                  style={
                    treatment_goals.treat_arches_both
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.treat_arches_both
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Both
                  </Text>
                </View>
              </View>
              <Text style={styles.title}>Midline : </Text>
              <View style={styles.container2}>
                <View style={styles.col_title}>
                  <Text style={styles.text_not_active}>U</Text>
                </View>
                <View
                  style={
                    treatment_goals.midline_upper_maintain
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.midline_upper_maintain
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Maintain
                  </Text>
                </View>
                <View
                  style={
                    treatment_goals.midline_upper_improve
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.midline_upper_improve
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Improve
                  </Text>
                </View>
                <View
                  style={
                    treatment_goals.midline_upper_center
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.midline_upper_center
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Center
                  </Text>
                </View>
              </View>
              <View style={styles.container2}>
                <View style={styles.col_title}>
                  <Text style={styles.text_not_active}>L</Text>
                </View>
                <View
                  style={
                    treatment_goals.midline_lower_maintain
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.midline_lower_maintain
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Maintain
                  </Text>
                </View>
                <View
                  style={
                    treatment_goals.midline_lower_improve
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.midline_lower_improve
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Improve
                  </Text>
                </View>
                <View
                  style={
                    treatment_goals.midline_lower_center
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.midline_lower_center
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Center
                  </Text>
                </View>
              </View>

              <Text style={styles.title}>Overjet : </Text>
              <View style={styles.container2}>
                <View style={styles.col_title}>
                  <Text style={styles.text_not_active}>{` `}</Text>
                </View>
                <View
                  style={
                    treatment_goals.overjet_maintain
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.overjet_maintain
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Maintain
                  </Text>
                </View>
                <View
                  style={
                    treatment_goals.overjet_improve
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.overjet_improve
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Improve
                  </Text>
                </View>
                <View
                  style={
                    treatment_goals.overjet_Ideal
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.overjet_Ideal
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Ideal
                  </Text>
                </View>
              </View>

              <Text style={styles.title}>Overbite : </Text>
              <View style={styles.container2}>
                <View style={styles.col_title}>
                  <Text style={styles.text_not_active}>{` `}</Text>
                </View>
                <View
                  style={
                    treatment_goals.overbite_maintain
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.overbite_maintain
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Maintain
                  </Text>
                </View>
                <View
                  style={
                    treatment_goals.overbite_improve
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.overbite_improve
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Improve
                  </Text>
                </View>
                <View
                  style={
                    treatment_goals.overbite_Ideal
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.overbite_Ideal
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Ideal
                  </Text>
                </View>
              </View>

              <Text style={styles.title}>Arch Form : </Text>
              <View style={styles.container2}>
                <View style={styles.col_title}>
                  <Text style={styles.text_not_active}>{` `}</Text>
                </View>
                <View
                  style={
                    treatment_goals.arch_form_maintain
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.arch_form_maintain
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Maintain
                  </Text>
                </View>
                <View
                  style={
                    treatment_goals.arch_form_improve
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.arch_form_improve
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Improve
                  </Text>
                </View>
                <View
                  style={
                    treatment_goals.arch_form_constrict
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.arch_form_constrict
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Constrict
                  </Text>
                </View>
              </View>

              <Text style={styles.title}>Procline : </Text>
              <View style={styles.container2}>
                <View style={styles.col_title}>
                  <Text style={styles.text_not_active}>U</Text>
                </View>
                <View
                  style={
                    treatment_goals.procline_upper_primary
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.procline_upper_primary
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Primary
                  </Text>
                </View>
                <View
                  style={
                    treatment_goals.procline_upper_if_needed
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.procline_upper_if_needed
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    if needed
                  </Text>
                </View>
                <View style={styles.emplty_col}>
                  <Text style={styles.text_not_active}>{` `}</Text>
                </View>
              </View>
              <View style={styles.container2}>
                <View style={styles.col_title}>
                  <Text style={styles.text_not_active}>L</Text>
                </View>
                <View
                  style={
                    treatment_goals.procline_lower_primary
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.procline_lower_primary
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Primary
                  </Text>
                </View>
                <View
                  style={
                    treatment_goals.procline_lower_if_needed
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.procline_lower_if_needed
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    if needed
                  </Text>
                </View>
                <View style={styles.emplty_col}>
                  <Text style={styles.text_not_active}>{` `}</Text>
                </View>
              </View>

              <Text style={styles.title}>Expand : </Text>
              <View style={styles.container2}>
                <View style={styles.col_title}>
                  <Text style={styles.text_not_active}>U</Text>
                </View>
                <View
                  style={
                    treatment_goals.expand_upper_primary
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.expand_upper_primary
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Primary
                  </Text>
                </View>
                <View
                  style={
                    treatment_goals.expand_upper_if_needed
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.expand_upper_if_needed
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    if needed
                  </Text>
                </View>
                <View style={styles.emplty_col}>
                  <Text style={styles.text_not_active}>{` `}</Text>
                </View>
              </View>
              <View style={styles.container2}>
                <View style={styles.col_title}>
                  <Text style={styles.text_not_active}>L</Text>
                </View>
                <View
                  style={
                    treatment_goals.expand_lower_primary
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.expand_lower_primary
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Primary
                  </Text>
                </View>
                <View
                  style={
                    treatment_goals.expand_lower_if_needed
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.expand_lower_if_needed
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    if needed
                  </Text>
                </View>
                <View style={styles.emplty_col}>
                  <Text style={styles.text_not_active}>{` `}</Text>
                </View>
              </View>

              <Text style={styles.title}>IPR : </Text>
              <View style={styles.container2}>
                <View style={styles.col_title}>
                  <Text style={styles.text_not_active}>U</Text>
                </View>
                <View
                  style={
                    treatment_goals.ipr_upper_primary
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.ipr_upper_primary
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Primary
                  </Text>
                </View>
                <View
                  style={
                    treatment_goals.ipr_upper_if_needed
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.ipr_upper_if_needed
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    if needed
                  </Text>
                </View>
                <View style={styles.emplty_col}>
                  <Text style={styles.text_not_active}>{` `}</Text>
                </View>
              </View>
              <View style={styles.container2}>
                <View style={styles.col_title}>
                  <Text style={styles.text_not_active}>L</Text>
                </View>
                <View
                  style={
                    treatment_goals.ipr_lower_primary
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.ipr_lower_primary
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Primary
                  </Text>
                </View>
                <View
                  style={
                    treatment_goals.ipr_lower_if_needed
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.ipr_lower_if_needed
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    if needed
                  </Text>
                </View>
                <View style={styles.emplty_col}>
                  <Text style={styles.text_not_active}>{` `}</Text>
                </View>
              </View>
            </View>
            <View style={styles.col_1}></View>
            <View style={styles.col_5}>
              <Text style={styles.title}>Extraction : </Text>
              <TeethPDF teeth={treatment_goals.Extraction} />
              <Text style={styles.title}>Crossbites : </Text>
              <TeethPDF teeth={treatment_goals.Crossbites} />
              <Text style={styles.title}>Leave space : </Text>
              <TeethPDF teeth={treatment_goals.LeaveSpace} />
              <Text style={styles.title}>Canine Relationship : </Text>
              <View style={styles.container2}>
                <View style={styles.col_title}>
                  <Text style={styles.text_not_active}>R</Text>
                </View>
                <View
                  style={
                    treatment_goals.canine_r_1
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.canine_r_1
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Class I
                  </Text>
                </View>
                <View
                  style={
                    treatment_goals.canine_r_2
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.canine_r_2
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Class II
                  </Text>
                </View>
                <View
                  style={
                    treatment_goals.canine_r_3
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.canine_r_3
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Class III
                  </Text>
                </View>
              </View>
              <View style={styles.container2}>
                <View style={styles.col_title}>
                  <Text style={styles.text_not_active}>L</Text>
                </View>
                <View
                  style={
                    treatment_goals.canine_l_1
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.canine_l_1
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Class I
                  </Text>
                </View>
                <View
                  style={
                    treatment_goals.canine_l_2
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.canine_l_2
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Class II
                  </Text>
                </View>
                <View
                  style={
                    treatment_goals.canine_l_3
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.canine_l_3
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Class III
                  </Text>
                </View>
              </View>

              <Text style={styles.title}>Molar Relationship : </Text>
              <View style={styles.container2}>
                <View style={styles.col_title}>
                  <Text style={styles.text_not_active}>R</Text>
                </View>
                <View
                  style={
                    treatment_goals.molar_r_1
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.molar_r_1
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Class I
                  </Text>
                </View>
                <View
                  style={
                    treatment_goals.molar_r_2
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.molar_r_2
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Class II
                  </Text>
                </View>
                <View
                  style={
                    treatment_goals.molar_r_3
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.molar_r_3
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Class III
                  </Text>
                </View>
              </View>
              <View style={styles.container2}>
                <View style={styles.col_title}>
                  <Text style={styles.text_not_active}>L</Text>
                </View>
                <View
                  style={
                    treatment_goals.molar_l_1
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.molar_l_1
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Class I
                  </Text>
                </View>
                <View
                  style={
                    treatment_goals.molar_l_2
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.molar_l_2
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Class II
                  </Text>
                </View>
                <View
                  style={
                    treatment_goals.molar_l_3
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.molar_l_3
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Class III
                  </Text>
                </View>
              </View>

              <Text style={styles.title}>How to Achieve A-P Goal : </Text>
              <View style={styles.container2}>
                <View style={styles.col_title}>
                  <Text style={styles.text_not_active}>U</Text>
                </View>
                <View
                  style={
                    treatment_goals.goal_upper_ipr
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.goal_upper_ipr
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    IPR 3-6
                  </Text>
                </View>
                <View
                  style={
                    treatment_goals.goal_upper_distalization
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.goal_upper_distalization
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Distalization
                  </Text>
                </View>
                <View
                  style={
                    treatment_goals.goal_upper_mezialization
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.goal_upper_mezialization
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Mezialization
                  </Text>
                </View>
              </View>
              <View style={styles.container2}>
                <View style={styles.col_title}>
                  <Text style={styles.text_not_active}>L</Text>
                </View>
                <View
                  style={
                    treatment_goals.goal_lower_ipr
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.goal_lower_ipr
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    IPR 3-6
                  </Text>
                </View>
                <View
                  style={
                    treatment_goals.goal_lower_distalization
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.goal_lower_distalization
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Distalization
                  </Text>
                </View>
                <View
                  style={
                    treatment_goals.goal_lower_mezialization
                      ? styles.green_btn_active
                      : styles.green_btn_not_active
                  }
                >
                  <Text
                    style={
                      treatment_goals.goal_lower_mezialization
                        ? styles.text_active
                        : styles.text_not_active
                    }
                  >
                    Mezialization
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </View>
      </Fragment>
    );
  }
}
