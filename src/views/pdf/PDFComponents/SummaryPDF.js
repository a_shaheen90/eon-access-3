import React, { Component, Fragment } from "react";
import { Text, StyleSheet, View } from "@react-pdf/renderer";
const styles = StyleSheet.create({
  row: {
    paddingTop: 10,
    alignItems: "center",
  },

  header: {
    fontSize: 12,
    textAlign: "left",
    color: "#00c7b1",
    width: "100%",
  },
  container: {
    flexDirection: "row",
  
    paddingBottom: 10,
    paddingTop: 5,
  },
  container2: {
    flexDirection: "row",
  },
  field: {
    flexDirection: "row",
    paddingTop: 10,
  },
  col: {
    flexDirection: "column",
    width: "50%",
  },
  col_4: {
    flexDirection: "column",
    width: "40%",
  },
  col_5: {
    flexDirection: "column",
    width: "50%",
  },
  col_1: {
    flexDirection: "column",
    width: "10%",
  },
  title: {
    fontSize: 10,
    textAlign: "left",
    color: "#00c7b1",
    padding: "10px 0 5px 0",
  },
  title2: {
    fontSize: 10,
    textAlign: "left",
    color: "#00c7b1",
  },
  info: {
    fontSize: 10,
    textAlign: "left",
  },
  image: {
    marginVertical: 10,
    width: "60%",
    height: "50%",
    backgroundColor: "red",
    borderRadius: 8,
  },

  green_btn_not_active: {
    flexGrow: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#d7f5f2",
    borderRadius: 8,
    fontSize: 8,
    padding: 2,
    margin: 3,
    flexDirection: "column",
    width: "30%",
  },
  col_title: {
    flexGrow: 1,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 8,
    fontSize: 8,
    padding: "0 5px 0 0",
    // margin: 1,
    flexDirection: "column",
    width: "10%",
  },
  green_btn_active: {
    flexGrow: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#00c7b1",
    borderRadius: 8,
    fontSize: 8,
    padding: 2,
    margin: 3,
    flexDirection: "column",
    width: "30%",
  },
  text_active: {
    fontSize: 10,
    color: "white",
    padding: 1,
  },
  text_not_active: {
    fontSize: 8,
    color: "#00c7b1",
    padding: 1,
  },
  emplty_col: {
    width: "30%",
  },
  text_area: {
    flexGrow: 1,
    borderColor: "#a0f0e7",
    borderWidth: 1,
    borderRadius: 2,
    fontSize: 8,
    padding: 2,
    margin: 3,
    minHeight: "50px",
    flexDirection: "column",
  },
  value: {
    padding: "10px",
  },
});
export default class SummaryPdf extends Component {
  constructor(props) {
    super(props);

    this.state = {
      summary_and_special_instructions:
        props.mappedData.treatmentPlan.summary_and_special_instructions,
    };
  }
  render() {
    const { summary_and_special_instructions } = this.state;
    return (
      <Fragment>
        <View style={styles.row}>
          <Text style={styles.header}>
            Treatment Summary & Special Instruction :
          </Text>
          <View style={styles.container}>
            <View style={styles.col_5}>
              <Text style={styles.title}>Treatment Summary : </Text>
              <View style={styles.container2}>
                <View style={styles.text_area}>
                  <Text style={styles.value}>
                    {summary_and_special_instructions.treatment_summary}
                  </Text>
                </View>
              </View>
            </View>
            <View style={styles.col_5}>
              <Text style={styles.title}>Special Instructions : </Text>
              <View style={styles.container2}>
                <View style={styles.text_area}>
                  <Text style={styles.value}>
                    {summary_and_special_instructions.special_instructions}
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </View>
      </Fragment>
    );
  }
}
