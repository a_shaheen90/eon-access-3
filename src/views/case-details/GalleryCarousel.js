import React, { Component } from "react";
import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption,
  Card,
  CardBody,
} from "reactstrap";
import ReactPanZoom from "react-image-pan-zoom-rotate";
class GalleryCarousel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeIndex: props.activeIndex,
    };
    this.myRef = React.createRef();
  }

  onExiting = () => {
    this.animating = false;
  };

  onExited = () => {
    this.animating = false;
  };

  next = () => {
    if (this.animating) {
      return;
    }
    const nextIndex =
      this.state.activeIndex === this.props.items.length - 1
        ? 0
        : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  };

  previous = () => {
    if (this.animating) {
      return;
    }
    const nextIndex =
      this.state.activeIndex === 0
        ? this.props.items.length - 1
        : this.state.activeIndex - 1;
    this.setState({ activeIndex: nextIndex });
  };

  goToIndex = (newIndex) => {
    if (this.animating) {
      return;
    }
    this.setState({ activeIndex: newIndex });
  };

  resetImage = () => {
    // this.myRef.current.state.zoom = 1;
    // this.myRef.current.zoomOut()
    //console.log(this.myRef.current);
  };
  render() {
    const activeIndex = this.state.activeIndex,
      slides = this.props.items.map((item, index) => {
        return (
          <CarouselItem
            className="custom-tag"
            onExiting={this.onExiting}
            onExited={this.onExited}
            key={index}
          >
            <ReactPanZoom image={item.src} alt={item.alt} ref={this.myRef} />

            {/* <div className="w-100 h-100">
              <img className="w-100 h-100" src={item.src} alt={item.caption} />
            </div> */}

            <CarouselCaption
              captionText={""}
              //   captionHeader={item.caption}
            />
          </CarouselItem>
        );
      });

    return (
      <div>
        <style>
          {`.custom-tag {
                max-width: 100%;
                height: 400px;
              }`}
        </style>
        {/* --------------------------------------------------------------------------------*/}
        {/* Start Inner Div*/}
        {/* --------------------------------------------------------------------------------*/}
        {/* <Row>
          <Col xs="12" md="12"> */}
        {/* --------------------------------------------------------------------------------*/}
        {/* Cariusel-1*/}
        {/* --------------------------------------------------------------------------------*/}
        <Card>
          <CardBody className="p-0">
            <Carousel
              activeIndex={activeIndex}
              next={this.next}
              previous={this.previous}
              className="gallery-carousel"
              interval={false}
            >
              <CarouselIndicators
                items={this.props.items}
                activeIndex={activeIndex}
                onClickHandler={this.goToIndex}
              />
              {slides}
              <CarouselControl
                direction="prev"
                directionText="Previous"
                onClickHandler={this.previous}
              />
              <CarouselControl
                direction="next"
                directionText="Next"
                onClickHandler={this.next}
              />
            </Carousel>
          </CardBody>
        </Card>

        <div
          className="reset-image"
          onClick={() => {
            this.resetImage();
          }}
        >
          {/* <i className="fa fa-refresh"></i> */}
          <i className="fas fa-sync"></i>
          {/* <svg
            width="24"
            height="24"
            viewBox="0 0 24 24"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M14 15L9 20L4 15"
              stroke="#000000"
              strokeWidth="2"
              strokeLinecap="round"
              strokeLinejoin="round"
            ></path>
            <path
              d="M20 4H13C10.7909 4 9 5.79086 9 8V20"
              stroke="#000000"
              strokeWidth="2"
              strokeLinecap="round"
              strokeLinejoin="round"
            ></path>
          </svg>
       
        */}
        </div>

        <h5 className="text-center pt-2">
          {this.props.items[activeIndex].alt}
        </h5>
        {/* </Col>
        </Row> */}
        {/* --------------------------------------------------------------------------------*/}
        {/* End Inner Div*/}
        {/* --------------------------------------------------------------------------------*/}
      </div>
    );
  }
}

export default GalleryCarousel;
