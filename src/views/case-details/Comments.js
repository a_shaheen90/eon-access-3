import React, { Component } from "react";
import Inbox from "../inbox";

export default class Comments extends Component {
  render() {
    return (
      <div className=" mt-4 ">
        {/*--------------------------------------------------------------------------------*/}
        {/* Inbox Component */}
        {/*--------------------------------------------------------------------------------*/}
        <Inbox
          caseId={this.props.caseId}
          comments={this.props.comments}
          formKey={"tabs"}
        />
      </div>
    );
  }
}
