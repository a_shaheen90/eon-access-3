import React, { Component } from "react";
import { Form, Input, Row, Col } from "reactstrap";
import PropTypes from "prop-types";
import { uploadFile, keyToUrl } from "../../helpers/s3";
import { postComment } from "../../api/api";
import { connect } from "react-redux";

const mapStateToProps = (state) => ({
  ...state,
});
class ChatMsgForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      msg: "",
      id: props.userReducer.currentUser.id,
      caseId: props.caseId,
    };
  }

  handleChatMsgChange = (e) => {
    this.setState({
      msg: e.target.value,
    });
  };

  onChatMsgSubmit = (e) => {
    e.preventDefault();
    e.stopPropagation();

    this.props.onClick(this.state.id, this.state.msg);
    this.setState({ msg: "" });
  };
  UploadFile = async (image, id) => {
    let extention = image.name.split(".");
    return uploadFile({
      name: `msg/${id}/${new Date().valueOf()}.${
        image.name.split(".")[extention.length - 1]
      }`,
      contentType: image.type,
      file: image,
    });
  };
  FileOnUpload = async (e, id) => {
    if (e.target.files[0]) {
      let reader = new FileReader();
      reader.addEventListener("load", (evt) => {
        this.setState({
          msg1: evt.currentTarget.result,
        });
      });
      reader.readAsDataURL(e.target.files[0]);

      const file = e.target ? e.target.files[0] : e.files[0];
      const { key } = await this.UploadFile(file, id);
      this.setState({
        msg: keyToUrl(key),
      });
    }
  };

  sendComment() {
    let data = {
      comment: {
        body: this.state.msg,
      },
    };
    postComment(
      this.state.id,
      this.state.caseId.toString(),
      JSON.stringify(data)
    ).then((res) => {
      this.props.onClick(this.props.id, this.state.msg);
      this.setState({ msg: "" });
    });
  }
  render() {
    return (
      <Form onSubmit={this.onChatMsgSubmit} className="card-body border-top">
        <Row>
          <Col xs="10" md="10" lg="10" className="pl-0 pr-0 m-0">
            <div className="d-flex">
              <Input
                type="textarea"
                rows={4}
                className="form-control mr-2"
                placeholder="Type your message"
                onChange={this.handleChatMsgChange}
                value={this.state.msg}
                required
              />
            </div>
            {/* <div className="title_active p-1">
              *Allowed file types are word documents, images, and PDFs.
            </div> */}
          </Col>
          <Col xs="2" md="2" lg="2" className=" align-self-center">
            <Row>
              <Col xs="12" md="12" lg="12" className="pl-1 pr-1">
                <input
                  type="file"
                  name="file"
                  id="file"
                  className="inputfile"
                  onChange={(e) => this.FileOnUpload(e, "file")}
                  multiple={false}
                  accept="image/*,.doc,.docx,.pdf"
                />
                <label htmlFor="file" className="w-100">
                  <div
                    className="btn btn-outline-secondary icon-small w-100"
                    type="button"
                  >
                    <i className="fa fa-paperclip" />
                  </div>
                </label>
              </Col>
              <Col xs="12" md="12" lg="12" className="pl-1 pr-1">
                <button
                  onClick={() => {
                    this.sendComment();
                  }}
                  className="btn btn-outline-secondary icon-small w-100"
                  type="button"
                  disabled={!this.state.msg}
                >
                  <i className="fas fa-paper-plane " />
                </button>
              </Col>
            </Row>
          </Col>
        </Row>
      </Form>
    );
  }
}

ChatMsgForm.propTypes = {
  onClick: PropTypes.func.isRequired,
};

export default connect(mapStateToProps)(ChatMsgForm);
